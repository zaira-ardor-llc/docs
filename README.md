# zaira-ardor-llc/docs

> Common documents and resources used in @zaira-ardor-llc projects.

## Usage

Find the resource you want to reference, copy its URL, and
paste into your project.

## Resources index

```shell
.
├── .gitlab-ci.yml
├── README.md
└── media
    ├── docs
    │   └── codacy-ebook-code-reviews.pdf
    │   └── codacy-ebook-metrics.pdf
    └── icons
        └── airbnb
        └── gitlab
```

## URLs

Reference these Gitlab-hosted icons.

1. _Example:_ ![City skyline view](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/city-skyline-view.svg?ref_type=heads) Airbnb's "City Skyline View" icon

   - **HTTP**:
     ```http
     URL: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/city-skyline-view.svg?ref_type=heads
     ```
   - **HTMLImage**:
     ```html
     <img src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/city-skyline-view.svg?ref_type=heads">
     ```
   - **Markdown Image**:
     ```markdown
     ![City skyline view](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/city-skyline-view.svg?ref_type=heads)
     ```
1. _Example:_ ![API](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads) Gitlab's "API" icon

    - **HTTP**:
      ```http
      URL: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads
      ```
    - **HTMLImage**:
      ```html
      <img src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads">
      ```
    - **Markdown Image**:
      ```markdown
      ![CAPI](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads)
      ```
