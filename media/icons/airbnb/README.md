<h1>Airbnb Icons</h1>

![Airbnb logo][airbnb-logo]{height="100" width="100"}

> A common location of Airbnb icons to use in our docuementation.

<h2>Table of Contents</h2>

[TOC]

## 1. Icons

There are 132 PNG icons and 133 SVG icons available.

1. <figure><figcaption>Airbnb logo<br><small>(<samp>airbnb.svg</samp>)</small></figcaption>

   ![Airbnb logo<br>airbnb][airbnb-logo]{height="24" width="24"}

   </figure>

1. <figure><figcaption>Air conditioning<br><small>(<samp>air-conditioning.svg</samp>)</small></figcaption>

   ![Air conditioning<br>air-conditioning][airbnb-air-conditioning]

   </figure>

1. <figure><figcaption>Arcade games<br><small>(<samp>arcade-games.svg</samp>)</small></figcaption>

   ![Arcade games<br>arcade-games][airbnb-arcade-games]

   </figure>

1. <figure><figcaption>Baby bath<br><small>(<samp>baby-bath.svg</samp>)</small></figcaption>

   ![Baby bath<br>baby-bath][airbnb-baby-bath]

   </figure>

1. <figure><figcaption>Baby monitor<br><small>(<samp>baby-monitor.svg</samp>)</small></figcaption>

   ![Baby monitor<br>baby-monitor][airbnb-baby-monitor]

   </figure>

1. <figure><figcaption>Baby safety gates<br><small>(<samp>baby-safety-gates.svg</samp>)</small></figcaption>

   ![Baby safety gates<br>baby-safety-gates][airbnb-baby-safety-gates]

   </figure>

1. <figure><figcaption>Babysitter recommendations<br><small>(<samp>babysitter-recommendations.svg</samp>)</small></figcaption>

   ![Babysitter recommendations<br>babysitter-recommendations][airbnb-babysitter-recommendations]

   </figure>

1. <figure><figcaption>Backyard<br><small>(<samp>backyard.svg</samp>)</small></figcaption>

   ![Backyard<br>backyard][airbnb-backyard]

   </figure>

1. <figure><figcaption>Baking sheet<br><small>(<samp>baking-sheet.svg</samp>)</small></figcaption>

   ![Baking sheet<br>baking-sheet][airbnb-baking-sheet]

   </figure>

1. <figure><figcaption>Barbecue utensils<br><small>(<samp>barbecue-utensils.svg</samp>)</small></figcaption>

   ![Barbecue utensils<br>barbecue-utensils][airbnb-barbecue-utensils]

   </figure>

1. <figure><figcaption>Bathtub<br><small>(<samp>bathtub.svg</samp>)</small></figcaption>

   ![Bathtub<br>bathtub][airbnb-bathtub]

   </figure>

1. <figure><figcaption>Batting cage<br><small>(<samp>batting-cage.svg</samp>)</small></figcaption>

   ![Batting cage<br>batting-cage][airbnb-batting-cage]

   </figure>

1. <figure><figcaption>BBQ grill<br><small>(<samp>bbq-grill.svg</samp>)</small></figcaption>

   ![BBQ grill<br>bbq-grill][airbnb-bbq-grill]

   </figure>

1. <figure><figcaption>Beach access<br><small>(<samp>beach-access.svg</samp>)</small></figcaption>

   ![Beach access<br>beach-access][airbnb-beach-access]

   </figure>

1. <figure><figcaption>Beach essentials<br><small>(<samp>beach-essentials.svg</samp>)</small></figcaption>

   ![Beach essentials<br>beach-essentials][airbnb-beach-essentials]

   </figure>

1. <figure><figcaption>Bed linens<br><small>(<samp>bed-linens.svg</samp>)</small></figcaption>

   ![Bed linens<br>bed-linens][airbnb-bed-linens]

   </figure>

1. <figure><figcaption>Bidet<br><small>(<samp>bidet.svg</samp>)</small></figcaption>

   ![Bidet<br>bidet][airbnb-bidet]

   </figure>

1. <figure><figcaption>Bikes<br><small>(<samp>bikes.svg</samp>)</small></figcaption>

   ![Bikes<br>bikes][airbnb-bikes]

   </figure>

1. <figure><figcaption>Blender<br><small>(<samp>blender.svg</samp>)</small></figcaption>

   ![Blender<br>blender][airbnb-blender]

   </figure>

1. <figure><figcaption>Board games<br><small>(<samp>board-games.svg</samp>)</small></figcaption>

   ![Board games<br>board-games][airbnb-board-games]

   </figure>

1. <figure><figcaption>Boat slip<br><small>(<samp>boat-slip.svg</samp>)</small></figcaption>

   ![Boat slip<br>boat-slip][airbnb-boat-slip]

   </figure>

1. <figure><figcaption>Body soap<br><small>(<samp>body-soap.svg</samp>)</small></figcaption>

   ![Body soap<br>body-soap][airbnb-body-soap]

   </figure>

1. <figure><figcaption>Books and reading material<br><small>(<samp>books-and-reading-material.svg</samp>)</small></figcaption>

   ![Books and reading material<br>books-and-reading-material][airbnb-books-and-reading-material]

   </figure>

1. <figure><figcaption>Bowling alley<br><small>(<samp>bowling-alley.svg</samp>)</small></figcaption>

   ![Bowling alley<br>bowling-alley][airbnb-bowling-alley]

   </figure>

1. <figure><figcaption>Bread maker<br><small>(<samp>bread-maker.svg</samp>)</small></figcaption>

   ![Bread maker<br>bread-maker][airbnb-bread-maker]

   </figure>

1. <figure><figcaption>Breakfast<br><small>(<samp>breakfast.svg</samp>)</small></figcaption>

   ![Breakfast<br>breakfast][airbnb-breakfast]

   </figure>

1. <figure><figcaption>Carbon monoxide alarm<br><small>(<samp>carbon-monoxide-alarm.svg</samp>)</small></figcaption>

   ![Carbon monoxide alarm<br>carbon-monoxide-alarm][airbnb-carbon-monoxide-alarm]

   </figure>

1. <figure><figcaption>Ceiling fan<br><small>(<samp>ceiling-fan.svg</samp>)</small></figcaption>

   ![Ceiling fan<br>ceiling-fan][airbnb-ceiling-fan]

   </figure>

1. <figure><figcaption>Changing table<br><small>(<samp>changing-table.svg</samp>)</small></figcaption>

   ![Changing table<br>changing-table][airbnb-changing-table]

   </figure>

1. <figure><figcaption>Children's playroom<br><small>(<samp>childrens-playroom.svg</samp>)</small></figcaption>

   ![Children's playroom<br>childrens-playroom][airbnb-childrens-playroom]

   </figure>

1. <figure><figcaption>Children’s bikes<br><small>(<samp>childrens-bikes.svg</samp>)</small></figcaption>

   ![Children’s bikes<br>childrens-bikes][airbnb-childrens-bikes]

   </figure>

1. <figure><figcaption>Children’s books and toys<br><small>(<samp>childrens-books-and-toys.svg</samp>)</small></figcaption>

   ![Children’s books and toys<br>childrens-books-and-toys][airbnb-childrens-books-and-toys]

   </figure>

1. <figure><figcaption>Children’s dinnerware<br><small>(<samp>childrens-dinnerware.svg</samp>)</small></figcaption>

   ![Children’s dinnerware<br>childrens-dinnerware][airbnb-childrens-dinnerware]

   </figure>

1. <figure><figcaption>City skyline view<br><small>(<samp>city-skyline-view.svg</samp>)</small></figcaption>

   ![City skyline view<br>city-skyline-view.svg][airbnb-city-skyline-view]

   </figure>

1. <figure><figcaption>Cleaning available during stay<br><small>(<samp>cleaning-available-during-stay.svg</samp>)</small></figcaption>

   ![Cleaning available during stay<br>cleaning-available-during-stay][airbnb-cleaning-available-during-stay]

   </figure>

1. <figure><figcaption>Cleaning products<br><small>(<samp>cleaning-products.svg</samp>)</small></figcaption>

   ![Cleaning products<br>cleaning-products][airbnb-cleaning-products]

   </figure>

1. <figure><figcaption>Climbing wall<br><small>(<samp>climbing-wall.svg</samp>)</small></figcaption>

   ![Climbing wall<br>climbing-wall][airbnb-climbing-wall]

   </figure>

1. <figure><figcaption>Clothing storage<br><small>(<samp>clothing-storage.svg</samp>)</small></figcaption>

   ![Clothing storage<br>clothing-storage][airbnb-clothing-storage]

   </figure>

1. <figure><figcaption>Coffee<br><small>(<samp>coffee.svg</samp>)</small></figcaption>

   ![Coffee<br>coffee][airbnb-coffee]

   </figure>

1. <figure><figcaption>Coffee maker<br><small>(<samp>coffee-maker.svg</samp>)</small></figcaption>

   ![Coffee maker<br>coffee-maker][airbnb-coffee-maker]

   </figure>

1. <figure><figcaption>Conditioner<br><small>(<samp>conditioner.svg</samp>)</small></figcaption>

   ![Conditioner<br>conditioner][airbnb-conditioner]

   </figure>

1. <figure><figcaption>Cooking basics<br><small>(<samp>cooking-basics.svg</samp>)</small></figcaption>

   ![Cooking basics<br>cooking-basics][airbnb-cooking-basics]

   </figure>

1. <figure><figcaption>Crib<br><small>(<samp>crib.svg</samp>)</small></figcaption>

   ![Crib<br>crib][airbnb-crib]

   </figure>

1. <figure><figcaption>Dedicated workspace<br><small>(<samp>dedicated-workspace.svg</samp>)</small></figcaption>

   ![Dedicated workspace<br>dedicated-workspace][airbnb-dedicated-workspace]

   </figure>

1. <figure><figcaption>Dining table<br><small>(<samp>dining-table.svg</samp>)</small></figcaption>

   ![Dining table<br>dining-table][airbnb-dining-table]

   </figure>

1. <figure><figcaption>Dishes and silverware<br><small>(<samp>dishes-and-silverware.svg</samp>)</small></figcaption>

   ![Dishes and silverware<br>dishes-and-silverware][airbnb-dishes-and-silverware]

   </figure>

1. <figure><figcaption>Dishwasher<br><small>(<samp>dishwasher.svg</samp>)</small></figcaption>

   ![Dishwasher<br>dishwasher][airbnb-dishwasher]

   </figure>

1. <figure><figcaption>Dryer<br><small>(<samp>dryer.svg</samp>)</small></figcaption>

   ![Dryer<br>dryer][airbnb-dryer]

   </figure>

1. <figure><figcaption>Drying rack for clothing<br><small>(<samp>drying-rack-for-clothing.svg</samp>)</small></figcaption>

   ![Drying rack for clothing<br>drying-rack-for-clothing][airbnb-drying-rack-for-clothing]

   </figure>

1. <figure><figcaption>Elevator<br><small>(<samp>elevator.svg</samp>)</small></figcaption>

   ![Elevator<br>elevator][airbnb-elevator]

   </figure>

1. <figure><figcaption>Essentials<br><small>(<samp>essentials.svg</samp>)</small></figcaption>

   ![Essentials<br>essentials][airbnb-essentials]

   </figure>

1. <figure><figcaption>Ethernet connection<br><small>(<samp>ethernet-connection.svg</samp>)</small></figcaption>

   ![Ethernet connection<br>ethernet-connection][airbnb-ethernet-connection]

   </figure>

1. <figure><figcaption>EV charger<br><small>(<samp>ev-charger.svg</samp>)</small></figcaption>

   ![EV charger<br>ev-charger][airbnb-ev-charger]

   </figure>

1. <figure><figcaption>Exercise equipment<br><small>(<samp>exercise-equipment.svg</samp>)</small></figcaption>

   ![Exercise equipment<br>exercise-equipment][airbnb-exercise-equipment]

   </figure>

1. <figure><figcaption>Extra pillows and blankets<br><small>(<samp>extra-pillows-and-blankets.svg</samp>)</small></figcaption>

   ![Extra pillows and blankets<br>extra-pillows-and-blankets][airbnb-extra-pillows-and-blankets]

   </figure>

1. <figure><figcaption>Fire extinguisher<br><small>(<samp>fire-extinguisher.svg</samp>)</small></figcaption>

   ![Fire extinguisher<br>fire-extinguisher][airbnb-fire-extinguisher]

   </figure>

1. <figure><figcaption>Fire pit<br><small>(<samp>fire-pit.svg</samp>)</small></figcaption>

   ![Fire pit<br>fire-pit][airbnb-fire-pit]

   </figure>

1. <figure><figcaption>Fire screen<br><small>(<samp>fire-screen.svg</samp>)</small></figcaption>

   ![Fire screen<br>fire-screen][airbnb-fire-screen]

   </figure>

1. <figure><figcaption>First aid kit<br><small>(<samp>first-aid-kit.svg</samp>)</small></figcaption>

   ![First aid kit<br>first-aid-kit][airbnb-first-aid-kit]

   </figure>

1. <figure><figcaption>Free parking on premises<br><small>(<samp>free-parking-on-premises.svg</samp>)</small></figcaption>

   ![Free parking on premises<br>free-parking-on-premises][airbnb-free-parking-on-premises]

   </figure>

1. <figure><figcaption>Free street parking<br><small>(<samp>free-street-parking.svg</samp>)</small></figcaption>

   ![Free street parking<br>free-street-parking][airbnb-free-street-parking]

   </figure>

1. <figure><figcaption>Freezer<br><small>(<samp>freezer.svg</samp>)</small></figcaption>

   ![Freezer<br>freezer][airbnb-freezer]

   </figure>

1. <figure><figcaption>Game console<br><small>(<samp>game-console.svg</samp>)</small></figcaption>

   ![Game console<br>game-console][airbnb-game-console]

   </figure>

1. <figure><figcaption>Gym<br><small>(<samp>gym.svg</samp>)</small></figcaption>

   ![Gym<br>gym][airbnb-gym]

   </figure>

1. <figure><figcaption>Hair dryer<br><small>(<samp>hair-dryer.svg</samp>)</small></figcaption>

   ![Hair dryer<br>hair-dryer][airbnb-hair-dryer]

   </figure>

1. <figure><figcaption>Hammock<br><small>(<samp>hammock.svg</samp>)</small></figcaption>

   ![Hammock<br>hammock][airbnb-hammock]

   </figure>

1. <figure><figcaption>Hangers<br><small>(<samp>hangers.svg</samp>)</small></figcaption>

   ![Hangers<br>hangers][airbnb-hangers]

   </figure>

1. <figure><figcaption>Heating<br><small>(<samp>heating.svg</samp>)</small></figcaption>

   ![Heating<br>heating][airbnb-heating]

   </figure>

1. <figure><figcaption>High chair<br><small>(<samp>high-chair.svg</samp>)</small></figcaption>

   ![High chair<br>high-chair][airbnb-high-chair]

   </figure>

1. <figure><figcaption>Hockey rink<br><small>(<samp>hockey-rink.svg</samp>)</small></figcaption>

   ![Hockey rink<br>hockey-rink][airbnb-hockey-rink]

   </figure>

1. <figure><figcaption>Hot tub<br><small>(<samp>hot-tub.svg</samp>)</small></figcaption>

   ![Hot tub<br>hot-tub][airbnb-hot-tub]

   </figure>

1. <figure><figcaption>Hot water<br><small>(<samp>hot-water.svg</samp>)</small></figcaption>

   ![Hot water<br>hot-water][airbnb-hot-water]

   </figure>

1. <figure><figcaption>Hot water kettle<br><small>(<samp>hot-water-kettle.svg</samp>)</small></figcaption>

   ![Hot water kettle<br>hot-water-kettle][airbnb-hot-water-kettle]

   </figure>

1. <figure><figcaption>Indoor fireplace<br><small>(<samp>indoor-fireplace.svg</samp>)</small></figcaption>

   ![Indoor fireplace<br>indoor-fireplace][airbnb-indoor-fireplace]

   </figure>

1. <figure><figcaption>Iron<br><small>(<samp>iron.svg</samp>)</small></figcaption>

   ![Iron<br>iron][airbnb-iron]

   </figure>

1. <figure><figcaption>Kayak<br><small>(<samp>kayak.svg</samp>)</small></figcaption>

   ![Kayak<br>kayak][airbnb-kayak]

   </figure>

1. <figure><figcaption>Kitchen<br><small>(<samp>kitchen.svg</samp>)</small></figcaption>

   ![Kitchen<br>kitchen][airbnb-kitchen]

   </figure>

1. <figure><figcaption>Kitchenette<br><small>(<samp>kitchenette.svg</samp>)</small></figcaption>

   ![Kitchenette<br>kitchenette][airbnb-kitchenette]

   </figure>

1. <figure><figcaption>Lake access<br><small>(<samp>lake-access.svg</samp>)</small></figcaption>

   ![Lake access<br>lake-access][airbnb-lake-access]

   </figure>

1. <figure><figcaption>Laser tag<br><small>(<samp>laser-tag.svg</samp>)</small></figcaption>

   ![Laser tag<br>laser-tag][airbnb-laser-tag]

   </figure>

1. <figure><figcaption>Laundromat nearby<br><small>(<samp>laundromat-nearby.svg</samp>)</small></figcaption>

   ![Laundromat nearby<br>laundromat-nearby][airbnb-laundromat-nearby]

   </figure>

1. <figure><figcaption>Life size games<br><small>(<samp>life-size-games.svg</samp>)</small></figcaption>

   ![Life size games<br>life-size-games][airbnb-life-size-games]

   </figure>

1. <figure><figcaption>Long term stays allowed<br><small>(<samp>long-term-stays-allowed.svg</samp>)</small></figcaption>

   ![Long term stays allowed<br>long-term-stays-allowed][airbnb-long-term-stays-allowed]

   </figure>

1. <figure><figcaption>Luggage dropoff allowed<br><small>(<samp>luggage-dropoff-allowed.svg</samp>)</small></figcaption>

   ![Luggage dropoff allowed<br>luggage-dropoff-allowed][airbnb-luggage-dropoff-allowed]

   </figure>

1. <figure><figcaption>Microwave<br><small>(<samp>microwave.svg</samp>)</small></figcaption>

   ![Microwave<br>microwave][airbnb-microwave]

   </figure>

1. <figure><figcaption>Mini fridge<br><small>(<samp>mini-fridge.svg</samp>)</small></figcaption>

   ![Mini fridge<br>mini-fridge][airbnb-mini-fridge]

   </figure>

1. <figure><figcaption>Mini golf<br><small>(<samp>mini-golf.svg</samp>)</small></figcaption>

   ![Mini golf<br>mini-golf][airbnb-mini-golf]

   </figure>

1. <figure><figcaption>Mosquito net<br><small>(<samp>mosquito-net.svg</samp>)</small></figcaption>

   ![Mosquito net<br>mosquito-net][airbnb-mosquito-net]

   </figure>

1. <figure><figcaption>Movie theater<br><small>(<samp>movie-theater.svg</samp>)</small></figcaption>

   ![Movie theater<br>movie-theater][airbnb-movie-theater]

   </figure>

1. <figure><figcaption>Outdoor dining area<br><small>(<samp>outdoor-dining-area.svg</samp>)</small></figcaption>

   ![Outdoor dining area<br>outdoor-dining-area][airbnb-outdoor-dining-area]

   </figure>

1. <figure><figcaption>Outdoor furniture<br><small>(<samp>outdoor-furniture.svg</samp>)</small></figcaption>

   ![Outdoor furniture<br>outdoor-furniture][airbnb-outdoor-furniture]

   </figure>

1. <figure><figcaption>Outdoor kitchen<br><small>(<samp>outdoor-kitchen.svg</samp>)</small></figcaption>

   ![Outdoor kitchen<br>outdoor-kitchen][airbnb-outdoor-kitchen]

   </figure>

1. <figure><figcaption>Outdoor playground<br><small>(<samp>outdoor-playground.svg</samp>)</small></figcaption>

   ![Outdoor playground<br>outdoor-playground][airbnb-outdoor-playground]

   </figure>

1. <figure><figcaption>Outdoor shower<br><small>(<samp>outdoor-shower.svg</samp>)</small></figcaption>

   ![Outdoor shower<br>outdoor-shower][airbnb-outdoor-shower]

   </figure>

1. <figure><figcaption>Outlet covers<br><small>(<samp>outlet-covers.svg</samp>)</small></figcaption>

   ![Outlet covers<br>outlet-covers][airbnb-outlet-covers]

   </figure>

1. <figure><figcaption>Oven<br><small>(<samp>oven.svg</samp>)</small></figcaption>

   ![Oven<br>oven][airbnb-oven]

   </figure>

1. <figure><figcaption>Pack ’n play/Travel crib<br><small>(<samp>pack-n-play-travel-crib.svg</samp>)</small></figcaption>

   ![Pack ’n play/Travel crib<br>pack-n-play-travel-crib][airbnb-pack-n-play-travel-crib]

   </figure>

1. <figure><figcaption>Paid parking off premises<br><small>(<samp>paid-parking-off-premises.svg</samp>)</small></figcaption>

   ![Paid parking off premises<br>paid-parking-off-premises][airbnb-paid-parking-off-premises]

   </figure>

1. <figure><figcaption>Paid parking on premises<br><small>(<samp>paid-parking-on-premises.svg</samp>)</small></figcaption>

   ![Paid parking on premises<br>paid-parking-on-premises][airbnb-paid-parking-on-premises]

   </figure>

1. <figure><figcaption>Patio or balcony<br><small>(<samp>patio-or-balcony.svg</samp>)</small></figcaption>

   ![Patio or balcony<br>patio-or-balcony][airbnb-patio-or-balcony]

   </figure>

1. <figure><figcaption>Piano<br><small>(<samp>piano.svg</samp>)</small></figcaption>

   ![Piano<br>piano][airbnb-piano]

   </figure>

1. <figure><figcaption>Ping pong table<br><small>(<samp>ping-pong-table.svg</samp>)</small></figcaption>

   ![Ping pong table<br>ping-pong-table][airbnb-ping-pong-table]

   </figure>

1. <figure><figcaption>Pocket wifi<br><small>(<samp>pocket-wifi.svg</samp>)</small></figcaption>

   ![Pocket wifi<br>pocket-wifi][airbnb-pocket-wifi]

   </figure>

1. <figure><figcaption>Pool<br><small>(<samp>pool.svg</samp>)</small></figcaption>

   ![Pool<br>pool][airbnb-pool]

   </figure>

1. <figure><figcaption>Pool table<br><small>(<samp>pool-table.svg</samp>)</small></figcaption>

   ![Pool table<br>pool-table][airbnb-pool-table]

   </figure>

1. <figure><figcaption>Portable fans<br><small>(<samp>portable-fans.svg</samp>)</small></figcaption>

   ![Portable fans<br>portable-fans][airbnb-portable-fans]

   </figure>

1. <figure><figcaption>Private entrance<br><small>(<samp>private-entrance.svg</samp>)</small></figcaption>

   ![Private entrance<br>private-entrance][airbnb-private-entrance]

   </figure>

1. <figure><figcaption>Private living room<br><small>(<samp>private-living-room.svg</samp>)</small></figcaption>

   ![Private living room<br>private-living-room][airbnb-private-living-room]

   </figure>

1. <figure><figcaption>Record player<br><small>(<samp>record-player.svg</samp>)</small></figcaption>

   ![Record player<br>record-player][airbnb-record-player]

   </figure>

1. <figure><figcaption>Refrigerator<br><small>(<samp>refrigerator.svg</samp>)</small></figcaption>

   ![Refrigerator<br>refrigerator][airbnb-refrigerator]

   </figure>

1. <figure><figcaption>Resort access<br><small>(<samp>resort-access.svg</samp>)</small></figcaption>

   ![Resort access<br>resort-access][airbnb-resort-access]

   </figure>

1. <figure><figcaption>Rice Maker<br><small>(<samp>rice-maker.svg</samp>)</small></figcaption>

   ![Rice Maker<br>rice-maker][airbnb-rice-maker]

   </figure>

1. <figure><figcaption>Room-darkening shades<br><small>(<samp>room-darkening-shades.svg</samp>)</small></figcaption>

   ![Room-darkening shades<br>room-darkening-shades][airbnb-room-darkening-shades]

   </figure>

1. <figure><figcaption>Safe<br><small>(<samp>safe.svg</samp>)</small></figcaption>

   ![Safe<br>safe][airbnb-safe]

   </figure>

1. <figure><figcaption>Sauna<br><small>(<samp>sauna.svg</samp>)</small></figcaption>

   ![Sauna<br>sauna][airbnb-sauna]

   </figure>

1. <figure><figcaption>Shampoo<br><small>(<samp>shampoo.svg</samp>)</small></figcaption>

   ![Shampoo<br>shampoo][airbnb-shampoo]

   </figure>

1. <figure><figcaption>Shower gel<br><small>(<samp>shower-gel.svg</samp>)</small></figcaption>

   ![Shower gel<br>shower-gel][airbnb-shower-gel]

   </figure>

1. <figure><figcaption>Single level home<br><small>(<samp>single-level-home.svg</samp>)</small></figcaption>

   ![Single level home<br>single-level-home][airbnb-single-level-home]

   </figure>

1. <figure><figcaption>Skate ramp<br><small>(<samp>skate-ramp.svg</samp>)</small></figcaption>

   ![Skate ramp<br>skate-ramp][airbnb-skate-ramp]

   </figure>

1. <figure><figcaption>Ski-in/Ski-out<br><small>(<samp>ski-in-ski-out.svg</samp>)</small></figcaption>

   ![Ski-in/Ski-out<br>ski-in-ski-out][airbnb-ski-in-ski-out]

   </figure>

1. <figure><figcaption>Smoke alarm<br><small>(<samp>smoke-alarm.svg</samp>)</small></figcaption>

   ![Smoke alarm<br>smoke-alarm][airbnb-smoke-alarm]

   </figure>

1. <figure><figcaption>Sound system<br><small>(<samp>sound-system.svg</samp>)</small></figcaption>

   ![Sound system<br>sound-system][airbnb-sound-system]

   </figure>

1. <figure><figcaption>Stove<br><small>(<samp>stove.svg</samp>)</small></figcaption>

   ![Stove<br>stove][airbnb-stove]

   </figure>

1. <figure><figcaption>Sun loungers<br><small>(<samp>sun-loungers.svg</samp>)</small></figcaption>

   ![Sun loungers<br>sun-loungers][airbnb-sun-loungers]

   </figure>

1. <figure><figcaption>Table corner guards<br><small>(<samp>table-corner-guards.svg</samp>)</small></figcaption>

   ![Table corner guards<br>table-corner-guards][airbnb-table-corner-guards]

   </figure>

1. <figure><figcaption>Theme room<br><small>(<samp>theme-room.svg</samp>)</small></figcaption>

   ![Theme room<br>theme-room][airbnb-theme-room]

   </figure>

1. <figure><figcaption>Toaster<br><small>(<samp>toaster.svg</samp>)</small></figcaption>

   ![Toaster<br>toaster][airbnb-toaster]

   </figure>

1. <figure><figcaption>Trash compactor<br><small>(<samp>trash-compactor.svg</samp>)</small></figcaption>

   ![Trash compactor<br>trash-compactor][airbnb-trash-compactor]

   </figure>

1. <figure><figcaption>TV<br><small>(<samp>tv.svg</samp>)</small></figcaption>

   ![TV<br>tv][airbnb-tv]

   </figure>

1. <figure><figcaption>Washer<br><small>(<samp>washer.svg</samp>)</small></figcaption>

   ![Washer<br>washer][airbnb-washer]

   </figure>

1. <figure><figcaption>Waterfront<br><small>(<samp>waterfront.svg</samp>)</small></figcaption>

   ![Waterfront<br>waterfront][airbnb-waterfront]

   </figure>

1. <figure><figcaption>Wifi<br><small>(<samp>wifi.svg</samp>)</small></figcaption>

   ![Wifi<br>wifi][airbnb-wifi]

   </figure>

1. <figure><figcaption>Window guards<br><small>(<samp>window-guards.svg</samp>)</small></figcaption>

   ![Window guards<br>window-guards][airbnb-window-guards]

   </figure>

1. <figure><figcaption>Wine glasses<br><small>(<samp>wine-glasses.svg</samp>)</small></figcaption>

   ![Wine glasses<br>wine-glasses][airbnb-wine-glasses]

   </figure>

## 2. Usage

### 2.1. How to display an icon

To use any of these icons, use the URL path

https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/{ICON_NAME}?ref_type=heads

replacing `{ICON_NAME}` with the file name of the icon you want to use.

+ _Example (SVG icons):_

    >>>
    Display the "Board games" icon.

    <samp>board-games.svg</samp>

    ```bash
    https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.svg?ref_type=heads
    
    ![Board games icon](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.svg?ref_type=heads)
    ```
    >>>

+ _Example (PNG icons):_

    >>>
    Display the "Board games" icon.

    <samp>board-games.png</samp>

    ```bash
    https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.png?ref_type=heads
    
    ![Board games icon](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.png?ref_type=heads)
    ```
    >>>

### 2.2. How to resize an icon with Markdown

Use `{ height="\d+" width="\d+" }`

where `\d+` is a number with one or more digits.

+ _Examples:_

    >>>
    Display the "Coffee maker" icon as 100 pixels high and 100 pixels wide.

    ```markdown
    ![Coffee maker](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee-maker.svg?ref_type=heads){ height="100" width="100" }
    ```
    >>>

    or use its reference:

    >>>
    ```markdown
    ![Coffee maker][airbnb-coffee-maker]{height="100" width="100"}
    <!-- OR reference the PNG version -->
    ![Coffee maker][airbnb-png-coffee-maker]{height="100" width="100"}
    ```
    >>>

+ _Renders:_

   > ![Coffee maker][airbnb-coffee-maker]{height="100" width="100"}

### 2.3. How to resize an HTMLImageElement

To resize an [HTMLImageElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement), 
set the `<img>`'s `height` and `width` attributes.

+ _Example (SVG icons):_

   >>>
   ```html
   <img alt="Bidet" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bidet.svg?ref_type=heads" 
        height="50" 
        width="50">
    ```
    >>>

+ _Renders:_

   >>>
   <img alt="Bidet" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bidet.svg?ref_type=heads" 
        height="50" 
        width="50">
   >>>

+ _Example (PNG icons):_

   >>>
   ```html
   <img alt="refrigerator" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/refrigerator.png?ref_type=heads" 
        height="50" 
        width="50">
    ```
    >>>

+ _Renders:_

   >>>
   <img alt="refrigerator" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/refrigerator.png?ref_type=heads" 
        height="50" 
        width="50">
   >>>

## 3. Icon references

Copy and paste these image reference definitions into your Airbnb markdown documents.

### 3.1. PNG icon references

```markdown
[airbnb-png-air-conditioning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/air-conditioning.png?ref_type=heads "Air conditioning"
[airbnb-png-arcade-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/arcade-games.png?ref_type=heads "Arcade games"
[airbnb-png-baby-bath]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-bath.png?ref_type=heads "Baby bath"
[airbnb-png-baby-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-monitor.png?ref_type=heads "Baby monitor"
[airbnb-png-baby-safety-gates]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-safety-gates.png?ref_type=heads "Baby safety gates"
[airbnb-png-babysitter-recommendations]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/babysitter-recommendations.png?ref_type=heads "Babysitter recommendations"
[airbnb-png-backyard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/backyard.png?ref_type=heads "Backyard"
[airbnb-png-baking-sheet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baking-sheet.png?ref_type=heads "Baking sheet"
[airbnb-png-barbecue-utensils]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/barbecue-utensils.png?ref_type=heads "Barbecue utensils"
[airbnb-png-bathtub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bathtub.png?ref_type=heads "Bathtub"
[airbnb-png-batting-cage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/batting-cage.png?ref_type=heads "Batting cage"
[airbnb-png-bbq-grill]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bbq-grill.png?ref_type=heads "BBQ grill"
[airbnb-png-beach-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-access.png?ref_type=heads "Beach access"
[airbnb-png-beach-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-essentials.png?ref_type=heads "Beach essentials"
[airbnb-png-bed-linens]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bed-linens.png?ref_type=heads "Bed linens"
[airbnb-png-bidet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bidet.png?ref_type=heads "Bidet"
[airbnb-png-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bikes.png?ref_type=heads "Bikes"
[airbnb-png-blender]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/blender.png?ref_type=heads "Blender"
[airbnb-png-board-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.png?ref_type=heads "Board games"
[airbnb-png-boat-slip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/boat-slip.png?ref_type=heads "Boat slip"
[airbnb-png-body-soap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/body-soap.png?ref_type=heads "Body soap"
[airbnb-png-books-and-reading-material]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/books-and-reading-material.png?ref_type=heads "Books and reading material"
[airbnb-png-bowling-alley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bowling-alley.png?ref_type=heads "Bowling alley"
[airbnb-png-bread-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bread-maker.png?ref_type=heads "Bread maker"
[airbnb-png-breakfast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/breakfast.png?ref_type=heads "Breakfast"
[airbnb-png-carbon-monoxide-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/carbon-monoxide-alarm.png?ref_type=heads "Carbon monoxide alarm"
[airbnb-png-ceiling-fan]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ceiling-fan.png?ref_type=heads "Ceiling fan"
[airbnb-png-changing-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/changing-table.png?ref_type=heads "Changing table"
[airbnb-png-childrens-playroom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-playroom.png?ref_type=heads "Children's playroom"
[airbnb-png-childrens-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-bikes.png?ref_type=heads "Children’s bikes"
[airbnb-png-childrens-books-and-toys]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-books-and-toys.png?ref_type=heads "Children’s books and toys"
[airbnb-png-childrens-dinnerware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-dinnerware.png?ref_type=heads "Children’s dinnerware"
[airbnb-png-cleaning-available-during-stay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-available-during-stay.png?ref_type=heads "Cleaning available during stay"
[airbnb-png-cleaning-products]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-products.png?ref_type=heads "Cleaning products"
[airbnb-png-climbing-wall]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/climbing-wall.png?ref_type=heads "Climbing wall"
[airbnb-png-clothing-storage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/clothing-storage.png?ref_type=heads "Clothing storage"
[airbnb-png-coffee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee.png?ref_type=heads "Coffee"
[airbnb-png-coffee-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee-maker.png?ref_type=heads "Coffee maker"
[airbnb-png-conditioner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/conditioner.png?ref_type=heads "Conditioner"
[airbnb-png-cooking-basics]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cooking-basics.png?ref_type=heads "Cooking basics"
[airbnb-png-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/crib.png?ref_type=heads "Crib"
[airbnb-png-dedicated-workspace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dedicated-workspace.png?ref_type=heads "Dedicated workspace"
[airbnb-png-dining-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dining-table.png?ref_type=heads "Dining table"
[airbnb-png-dishes-and-silverware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishes-and-silverware.png?ref_type=heads "Dishes and silverware"
[airbnb-png-dishwasher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishwasher.png?ref_type=heads "Dishwasher"
[airbnb-png-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dryer.png?ref_type=heads "Dryer"
[airbnb-png-drying-rack-for-clothing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/drying-rack-for-clothing.png?ref_type=heads "Drying rack for clothing"
[airbnb-png-elevator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/elevator.png?ref_type=heads "Elevator"
[airbnb-png-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/essentials.png?ref_type=heads "Essentials"
[airbnb-png-ethernet-connection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ethernet-connection.png?ref_type=heads "Ethernet connection"
[airbnb-png-ev-charger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ev-charger.png?ref_type=heads "EV charger"
[airbnb-png-exercise-equipment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/exercise-equipment.png?ref_type=heads "Exercise equipment"
[airbnb-png-extra-pillows-and-blankets]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/extra-pillows-and-blankets.png?ref_type=heads "Extra pillows and blankets"
[airbnb-png-fire-extinguisher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-extinguisher.png?ref_type=heads "Fire extinguisher"
[airbnb-png-fire-pit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-pit.png?ref_type=heads "Fire pit"
[airbnb-png-fire-screen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-screen.png?ref_type=heads "Fire screen"
[airbnb-png-first-aid-kit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/first-aid-kit.png?ref_type=heads "First aid kit"
[airbnb-png-free-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-parking-on-premises.png?ref_type=heads "Free parking on premises"
[airbnb-png-free-street-parking]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-street-parking.png?ref_type=heads "Free street parking"
[airbnb-png-freezer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/freezer.png?ref_type=heads "Freezer"
[airbnb-png-game-console]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/game-console.png?ref_type=heads "Game console"
[airbnb-png-gym]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/gym.png?ref_type=heads "Gym"
[airbnb-png-hair-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hair-dryer.png?ref_type=heads "Hair dryer"
[airbnb-png-hammock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hammock.png?ref_type=heads "Hammock"
[airbnb-png-hangers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hangers.png?ref_type=heads "Hangers"
[airbnb-png-heating]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/heating.png?ref_type=heads "Heating"
[airbnb-png-high-chair]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/high-chair.png?ref_type=heads "High chair"
[airbnb-png-hockey-rink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hockey-rink.png?ref_type=heads "Hockey rink"
[airbnb-png-hot-tub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-tub.png?ref_type=heads "Hot tub"
[airbnb-png-hot-water]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water.png?ref_type=heads "Hot water"
[airbnb-png-hot-water-kettle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water-kettle.png?ref_type=heads "Hot water kettle"
[airbnb-png-indoor-fireplace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/indoor-fireplace.png?ref_type=heads "Indoor fireplace"
[airbnb-png-iron]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/iron.png?ref_type=heads "Iron"
[airbnb-png-kayak]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kayak.png?ref_type=heads "Kayak"
[airbnb-png-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchen.png?ref_type=heads "Kitchen"
[airbnb-png-kitchenette]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchenette.png?ref_type=heads "Kitchenette"
[airbnb-png-lake-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/lake-access.png?ref_type=heads "Lake access"
[airbnb-png-laser-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laser-tag.png?ref_type=heads "Laser tag"
[airbnb-png-laundromat-nearby]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laundromat-nearby.png?ref_type=heads "Laundromat nearby"
[airbnb-png-life-size-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/life-size-games.png?ref_type=heads "Life size games"
[airbnb-png-long-term-stays-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/long-term-stays-allowed.png?ref_type=heads "Long term stays allowed"
[airbnb-png-luggage-dropoff-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/luggage-dropoff-allowed.png?ref_type=heads "Luggage dropoff allowed"
[airbnb-png-microwave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/microwave.png?ref_type=heads "Microwave"
[airbnb-png-mini-fridge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-fridge.png?ref_type=heads "Mini fridge"
[airbnb-png-mini-golf]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-golf.png?ref_type=heads "Mini golf"
[airbnb-png-mosquito-net]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mosquito-net.png?ref_type=heads "Mosquito net"
[airbnb-png-movie-theater]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/movie-theater.png?ref_type=heads "Movie theater"
[airbnb-png-outdoor-dining-area]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-dining-area.png?ref_type=heads "Outdoor dining area"
[airbnb-png-outdoor-furniture]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-furniture.png?ref_type=heads "Outdoor furniture"
[airbnb-png-outdoor-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-kitchen.png?ref_type=heads "Outdoor kitchen"
[airbnb-png-outdoor-playground]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-playground.png?ref_type=heads "Outdoor playground"
[airbnb-png-outdoor-shower]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-shower.png?ref_type=heads "Outdoor shower"
[airbnb-png-outlet-covers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outlet-covers.png?ref_type=heads "Outlet covers"
[airbnb-png-oven]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/oven.png?ref_type=heads "Oven"
[airbnb-png-pack-n-play-travel-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pack-n-play-travel-crib.png?ref_type=heads "Pack ’n play/Travel crib"
[airbnb-png-paid-parking-off-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-off-premises.png?ref_type=heads "Paid parking off premises"
[airbnb-png-paid-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-on-premises.png?ref_type=heads "Paid parking on premises"
[airbnb-png-patio-or-balcony]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/patio-or-balcony.png?ref_type=heads "Patio or balcony"
[airbnb-png-piano]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/piano.png?ref_type=heads "Piano"
[airbnb-png-ping-pong-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ping-pong-table.png?ref_type=heads "Ping pong table"
[airbnb-png-pocket-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pocket-wifi.png?ref_type=heads "Pocket wifi"
[airbnb-png-pool]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool.png?ref_type=heads "Pool"
[airbnb-png-pool-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool-table.png?ref_type=heads "Pool table"
[airbnb-png-portable-fans]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/portable-fans.png?ref_type=heads "Portable fans"
[airbnb-png-private-entrance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-entrance.png?ref_type=heads "Private entrance"
[airbnb-png-private-living-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-living-room.png?ref_type=heads "Private living room"
[airbnb-png-record-player]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/record-player.png?ref_type=heads "Record player"
[airbnb-png-refrigerator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/refrigerator.png?ref_type=heads "Refrigerator"
[airbnb-png-resort-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/resort-access.png?ref_type=heads "Resort access"
[airbnb-png-rice-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/rice-maker.png?ref_type=heads "Rice Maker"
[airbnb-png-room-darkening-shades]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/room-darkening-shades.png?ref_type=heads "Room-darkening shades"
[airbnb-png-safe]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/safe.png?ref_type=heads "Safe"
[airbnb-png-sauna]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sauna.png?ref_type=heads "Sauna"
[airbnb-png-shampoo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shampoo.png?ref_type=heads "Shampoo"
[airbnb-png-shower-gel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shower-gel.png?ref_type=heads "Shower gel"
[airbnb-png-single-level-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/single-level-home.png?ref_type=heads "Single level home"
[airbnb-png-skate-ramp]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/skate-ramp.png?ref_type=heads "Skate ramp"
[airbnb-png-ski-in-ski-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ski-in-ski-out.png?ref_type=heads "Ski-in/Ski-out"
[airbnb-png-smoke-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/smoke-alarm.png?ref_type=heads "Smoke alarm"
[airbnb-png-sound-system]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sound-system.png?ref_type=heads "Sound system"
[airbnb-png-stove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/stove.png?ref_type=heads "Stove"
[airbnb-png-sun-loungers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sun-loungers.png?ref_type=heads "Sun loungers"
[airbnb-png-table-corner-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/table-corner-guards.png?ref_type=heads "Table corner guards"
[airbnb-png-theme-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/theme-room.png?ref_type=heads "Theme room"
[airbnb-png-toaster]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/toaster.png?ref_type=heads "Toaster"
[airbnb-png-trash-compactor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/trash-compactor.png?ref_type=heads "Trash compactor"
[airbnb-png-tv]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/tv.png?ref_type=heads "TV"
[airbnb-png-washer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/washer.png?ref_type=heads "Washer"
[airbnb-png-waterfront]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/waterfront.png?ref_type=heads "Waterfront"
[airbnb-png-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wifi.png?ref_type=heads "Wifi"
[airbnb-png-window-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/window-guards.png?ref_type=heads "Window guards"
[airbnb-png-wine-glasses]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wine-glasses.png?ref_type=heads "Wine glasses"
```

### 3.2. SVG icon references

```markdown
[airbnb-logo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/airbnb.svg?ref_type=heads "Airbnb logo"
[airbnb-air-conditioning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/air-conditioning.svg?ref_type=heads "Air conditioning"
[airbnb-arcade-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/arcade-games.svg?ref_type=heads "Arcade games"
[airbnb-baby-bath]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-bath.svg?ref_type=heads "Baby bath"
[airbnb-baby-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-monitor.svg?ref_type=heads "Baby monitor"
[airbnb-baby-safety-gates]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-safety-gates.svg?ref_type=heads "Baby safety gates"
[airbnb-babysitter-recommendations]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/babysitter-recommendations.svg?ref_type=heads "Babysitter recommendations"
[airbnb-backyard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/backyard.svg?ref_type=heads "Backyard"
[airbnb-baking-sheet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baking-sheet.svg?ref_type=heads "Baking sheet"
[airbnb-barbecue-utensils]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/barbecue-utensils.svg?ref_type=heads "Barbecue utensils"
[airbnb-bathtub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bathtub.svg?ref_type=heads "Bathtub"
[airbnb-batting-cage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/batting-cage.svg?ref_type=heads "Batting cage"
[airbnb-bbq-grill]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bbq-grill.svg?ref_type=heads "BBQ grill"
[airbnb-beach-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-access.svg?ref_type=heads "Beach access"
[airbnb-beach-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-essentials.svg?ref_type=heads "Beach essentials"
[airbnb-bed-linens]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bed-linens.svg?ref_type=heads "Bed linens"
[airbnb-bidet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bidet.svg?ref_type=heads "Bidet"
[airbnb-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bikes.svg?ref_type=heads "Bikes"
[airbnb-blender]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/blender.svg?ref_type=heads "Blender"
[airbnb-board-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.svg?ref_type=heads "Board games"
[airbnb-boat-slip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/boat-slip.svg?ref_type=heads "Boat slip"
[airbnb-body-soap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/body-soap.svg?ref_type=heads "Body soap"
[airbnb-books-and-reading-material]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/books-and-reading-material.svg?ref_type=heads "Books and reading material"
[airbnb-bowling-alley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bowling-alley.svg?ref_type=heads "Bowling alley"
[airbnb-bread-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bread-maker.svg?ref_type=heads "Bread maker"
[airbnb-breakfast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/breakfast.svg?ref_type=heads "Breakfast"
[airbnb-carbon-monoxide-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/carbon-monoxide-alarm.svg?ref_type=heads "Carbon monoxide alarm"
[airbnb-ceiling-fan]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ceiling-fan.svg?ref_type=heads "Ceiling fan"
[airbnb-changing-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/changing-table.svg?ref_type=heads "Changing table"
[airbnb-childrens-playroom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-playroom.svg?ref_type=heads "Children's playroom"
[airbnb-childrens-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-bikes.svg?ref_type=heads "Children’s bikes"
[airbnb-childrens-books-and-toys]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-books-and-toys.svg?ref_type=heads "Children’s books and toys"
[airbnb-childrens-dinnerware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-dinnerware.svg?ref_type=heads "Children’s dinnerware"
[airbnb-cleaning-available-during-stay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-available-during-stay.svg?ref_type=heads "Cleaning available during stay"
[airbnb-cleaning-products]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-products.svg?ref_type=heads "Cleaning products"
[airbnb-climbing-wall]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/climbing-wall.svg?ref_type=heads "Climbing wall"
[airbnb-clothing-storage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/clothing-storage.svg?ref_type=heads "Clothing storage"
[airbnb-coffee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee.svg?ref_type=heads "Coffee"
[airbnb-coffee-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee-maker.svg?ref_type=heads "Coffee maker"
[airbnb-conditioner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/conditioner.svg?ref_type=heads "Conditioner"
[airbnb-cooking-basics]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cooking-basics.svg?ref_type=heads "Cooking basics"
[airbnb-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/crib.svg?ref_type=heads "Crib"
[airbnb-dedicated-workspace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dedicated-workspace.svg?ref_type=heads "Dedicated workspace"
[airbnb-dining-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dining-table.svg?ref_type=heads "Dining table"
[airbnb-dishes-and-silverware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishes-and-silverware.svg?ref_type=heads "Dishes and silverware"
[airbnb-dishwasher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishwasher.svg?ref_type=heads "Dishwasher"
[airbnb-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dryer.svg?ref_type=heads "Dryer"
[airbnb-drying-rack-for-clothing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/drying-rack-for-clothing.svg?ref_type=heads "Drying rack for clothing"
[airbnb-elevator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/elevator.svg?ref_type=heads "Elevator"
[airbnb-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/essentials.svg?ref_type=heads "Essentials"
[airbnb-ethernet-connection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ethernet-connection.svg?ref_type=heads "Ethernet connection"
[airbnb-ev-charger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ev-charger.svg?ref_type=heads "EV charger"
[airbnb-exercise-equipment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/exercise-equipment.svg?ref_type=heads "Exercise equipment"
[airbnb-extra-pillows-and-blankets]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/extra-pillows-and-blankets.svg?ref_type=heads "Extra pillows and blankets"
[airbnb-fire-extinguisher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-extinguisher.svg?ref_type=heads "Fire extinguisher"
[airbnb-fire-pit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-pit.svg?ref_type=heads "Fire pit"
[airbnb-fire-screen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-screen.svg?ref_type=heads "Fire screen"
[airbnb-first-aid-kit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/first-aid-kit.svg?ref_type=heads "First aid kit"
[airbnb-free-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-parking-on-premises.svg?ref_type=heads "Free parking on premises"
[airbnb-free-street-parking]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-street-parking.svg?ref_type=heads "Free street parking"
[airbnb-freezer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/freezer.svg?ref_type=heads "Freezer"
[airbnb-game-console]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/game-console.svg?ref_type=heads "Game console"
[airbnb-gym]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/gym.svg?ref_type=heads "Gym"
[airbnb-hair-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hair-dryer.svg?ref_type=heads "Hair dryer"
[airbnb-hammock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hammock.svg?ref_type=heads "Hammock"
[airbnb-hangers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hangers.svg?ref_type=heads "Hangers"
[airbnb-heating]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/heating.svg?ref_type=heads "Heating"
[airbnb-high-chair]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/high-chair.svg?ref_type=heads "High chair"
[airbnb-hockey-rink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hockey-rink.svg?ref_type=heads "Hockey rink"
[airbnb-hot-tub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-tub.svg?ref_type=heads "Hot tub"
[airbnb-hot-water]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water.svg?ref_type=heads "Hot water"
[airbnb-hot-water-kettle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water-kettle.svg?ref_type=heads "Hot water kettle"
[airbnb-indoor-fireplace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/indoor-fireplace.svg?ref_type=heads "Indoor fireplace"
[airbnb-iron]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/iron.svg?ref_type=heads "Iron"
[airbnb-kayak]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kayak.svg?ref_type=heads "Kayak"
[airbnb-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchen.svg?ref_type=heads "Kitchen"
[airbnb-kitchenette]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchenette.svg?ref_type=heads "Kitchenette"
[airbnb-lake-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/lake-access.svg?ref_type=heads "Lake access"
[airbnb-laser-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laser-tag.svg?ref_type=heads "Laser tag"
[airbnb-laundromat-nearby]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laundromat-nearby.svg?ref_type=heads "Laundromat nearby"
[airbnb-life-size-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/life-size-games.svg?ref_type=heads "Life size games"
[airbnb-long-term-stays-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/long-term-stays-allowed.svg?ref_type=heads "Long term stays allowed"
[airbnb-luggage-dropoff-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/luggage-dropoff-allowed.svg?ref_type=heads "Luggage dropoff allowed"
[airbnb-microwave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/microwave.svg?ref_type=heads "Microwave"
[airbnb-mini-fridge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-fridge.svg?ref_type=heads "Mini fridge"
[airbnb-mini-golf]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-golf.svg?ref_type=heads "Mini golf"
[airbnb-mosquito-net]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mosquito-net.svg?ref_type=heads "Mosquito net"
[airbnb-movie-theater]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/movie-theater.svg?ref_type=heads "Movie theater"
[airbnb-outdoor-dining-area]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-dining-area.svg?ref_type=heads "Outdoor dining area"
[airbnb-outdoor-furniture]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-furniture.svg?ref_type=heads "Outdoor furniture"
[airbnb-outdoor-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-kitchen.svg?ref_type=heads "Outdoor kitchen"
[airbnb-outdoor-playground]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-playground.svg?ref_type=heads "Outdoor playground"
[airbnb-outdoor-shower]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-shower.svg?ref_type=heads "Outdoor shower"
[airbnb-outlet-covers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outlet-covers.svg?ref_type=heads "Outlet covers"
[airbnb-oven]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/oven.svg?ref_type=heads "Oven"
[airbnb-pack-n-play-travel-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pack-n-play-travel-crib.svg?ref_type=heads "Pack ’n play/Travel crib"
[airbnb-paid-parking-off-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-off-premises.svg?ref_type=heads "Paid parking off premises"
[airbnb-paid-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-on-premises.svg?ref_type=heads "Paid parking on premises"
[airbnb-patio-or-balcony]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/patio-or-balcony.svg?ref_type=heads "Patio or balcony"
[airbnb-piano]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/piano.svg?ref_type=heads "Piano"
[airbnb-ping-pong-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ping-pong-table.svg?ref_type=heads "Ping pong table"
[airbnb-pocket-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pocket-wifi.svg?ref_type=heads "Pocket wifi"
[airbnb-pool]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool.svg?ref_type=heads "Pool"
[airbnb-pool-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool-table.svg?ref_type=heads "Pool table"
[airbnb-portable-fans]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/portable-fans.svg?ref_type=heads "Portable fans"
[airbnb-private-entrance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-entrance.svg?ref_type=heads "Private entrance"
[airbnb-private-living-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-living-room.svg?ref_type=heads "Private living room"
[airbnb-record-player]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/record-player.svg?ref_type=heads "Record player"
[airbnb-refrigerator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/refrigerator.svg?ref_type=heads "Refrigerator"
[airbnb-resort-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/resort-access.svg?ref_type=heads "Resort access"
[airbnb-rice-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/rice-maker.svg?ref_type=heads "Rice Maker"
[airbnb-room-darkening-shades]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/room-darkening-shades.svg?ref_type=heads "Room-darkening shades"
[airbnb-safe]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/safe.svg?ref_type=heads "Safe"
[airbnb-sauna]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sauna.svg?ref_type=heads "Sauna"
[airbnb-shampoo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shampoo.svg?ref_type=heads "Shampoo"
[airbnb-shower-gel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shower-gel.svg?ref_type=heads "Shower gel"
[airbnb-single-level-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/single-level-home.svg?ref_type=heads "Single level home"
[airbnb-skate-ramp]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/skate-ramp.svg?ref_type=heads "Skate ramp"
[airbnb-ski-in-ski-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ski-in-ski-out.svg?ref_type=heads "Ski-in/Ski-out"
[airbnb-smoke-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/smoke-alarm.svg?ref_type=heads "Smoke alarm"
[airbnb-sound-system]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sound-system.svg?ref_type=heads "Sound system"
[airbnb-stove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/stove.svg?ref_type=heads "Stove"
[airbnb-sun-loungers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sun-loungers.svg?ref_type=heads "Sun loungers"
[airbnb-table-corner-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/table-corner-guards.svg?ref_type=heads "Table corner guards"
[airbnb-theme-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/theme-room.svg?ref_type=heads "Theme room"
[airbnb-toaster]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/toaster.svg?ref_type=heads "Toaster"
[airbnb-trash-compactor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/trash-compactor.svg?ref_type=heads "Trash compactor"
[airbnb-tv]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/tv.svg?ref_type=heads "TV"
[airbnb-washer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/washer.svg?ref_type=heads "Washer"
[airbnb-waterfront]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/waterfront.svg?ref_type=heads "Waterfront"
[airbnb-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wifi.svg?ref_type=heads "Wifi"
[airbnb-window-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/window-guards.svg?ref_type=heads "Window guards"
[airbnb-wine-glasses]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wine-glasses.svg?ref_type=heads "Wine glasses"
```

[airbnb-logo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/airbnb.svg?ref_type=heads "Airbnb logo"
[airbnb-air-conditioning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/air-conditioning.svg?ref_type=heads "Air conditioning"
[airbnb-arcade-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/arcade-games.svg?ref_type=heads "Arcade games"
[airbnb-baby-bath]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-bath.svg?ref_type=heads "Baby bath"
[airbnb-baby-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-monitor.svg?ref_type=heads "Baby monitor"
[airbnb-baby-safety-gates]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baby-safety-gates.svg?ref_type=heads "Baby safety gates"
[airbnb-babysitter-recommendations]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/babysitter-recommendations.svg?ref_type=heads "Babysitter recommendations"
[airbnb-backyard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/backyard.svg?ref_type=heads "Backyard"
[airbnb-baking-sheet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/baking-sheet.svg?ref_type=heads "Baking sheet"
[airbnb-barbecue-utensils]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/barbecue-utensils.svg?ref_type=heads "Barbecue utensils"
[airbnb-bathtub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bathtub.svg?ref_type=heads "Bathtub"
[airbnb-batting-cage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/batting-cage.svg?ref_type=heads "Batting cage"
[airbnb-bbq-grill]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bbq-grill.svg?ref_type=heads "BBQ grill"
[airbnb-beach-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-access.svg?ref_type=heads "Beach access"
[airbnb-beach-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/beach-essentials.svg?ref_type=heads "Beach essentials"
[airbnb-bed-linens]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bed-linens.svg?ref_type=heads "Bed linens"
[airbnb-bidet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bidet.svg?ref_type=heads "Bidet"
[airbnb-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bikes.svg?ref_type=heads "Bikes"
[airbnb-blender]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/blender.svg?ref_type=heads "Blender"
[airbnb-board-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/board-games.svg?ref_type=heads "Board games"
[airbnb-boat-slip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/boat-slip.svg?ref_type=heads "Boat slip"
[airbnb-body-soap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/body-soap.svg?ref_type=heads "Body soap"
[airbnb-books-and-reading-material]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/books-and-reading-material.svg?ref_type=heads "Books and reading material"
[airbnb-bowling-alley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bowling-alley.svg?ref_type=heads "Bowling alley"
[airbnb-bread-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/bread-maker.svg?ref_type=heads "Bread maker"
[airbnb-breakfast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/breakfast.svg?ref_type=heads "Breakfast"
[airbnb-carbon-monoxide-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/carbon-monoxide-alarm.svg?ref_type=heads "Carbon monoxide alarm"
[airbnb-ceiling-fan]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ceiling-fan.svg?ref_type=heads "Ceiling fan"
[airbnb-changing-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/changing-table.svg?ref_type=heads "Changing table"
[airbnb-childrens-playroom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-playroom.svg?ref_type=heads "Children's playroom"
[airbnb-childrens-bikes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-bikes.svg?ref_type=heads "Children’s bikes"
[airbnb-childrens-books-and-toys]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-books-and-toys.svg?ref_type=heads "Children’s books and toys"
[airbnb-childrens-dinnerware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/childrens-dinnerware.svg?ref_type=heads "Children’s dinnerware"
[airbnb-city-skyline-view]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/city-skyline-view.svg?ref_type=heads "City skyline view"
[airbnb-cleaning-available-during-stay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-available-during-stay.svg?ref_type=heads "Cleaning available during stay"
[airbnb-cleaning-products]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cleaning-products.svg?ref_type=heads "Cleaning products"
[airbnb-climbing-wall]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/climbing-wall.svg?ref_type=heads "Climbing wall"
[airbnb-clothing-storage]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/clothing-storage.svg?ref_type=heads "Clothing storage"
[airbnb-coffee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee.svg?ref_type=heads "Coffee"
[airbnb-coffee-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/coffee-maker.svg?ref_type=heads "Coffee maker"
[airbnb-conditioner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/conditioner.svg?ref_type=heads "Conditioner"
[airbnb-cooking-basics]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/cooking-basics.svg?ref_type=heads "Cooking basics"
[airbnb-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/crib.svg?ref_type=heads "Crib"
[airbnb-dedicated-workspace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dedicated-workspace.svg?ref_type=heads "Dedicated workspace"
[airbnb-dining-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dining-table.svg?ref_type=heads "Dining table"
[airbnb-dishes-and-silverware]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishes-and-silverware.svg?ref_type=heads "Dishes and silverware"
[airbnb-dishwasher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dishwasher.svg?ref_type=heads "Dishwasher"
[airbnb-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/dryer.svg?ref_type=heads "Dryer"
[airbnb-drying-rack-for-clothing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/drying-rack-for-clothing.svg?ref_type=heads "Drying rack for clothing"
[airbnb-elevator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/elevator.svg?ref_type=heads "Elevator"
[airbnb-essentials]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/essentials.svg?ref_type=heads "Essentials"
[airbnb-ethernet-connection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ethernet-connection.svg?ref_type=heads "Ethernet connection"
[airbnb-ev-charger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ev-charger.svg?ref_type=heads "EV charger"
[airbnb-exercise-equipment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/exercise-equipment.svg?ref_type=heads "Exercise equipment"
[airbnb-extra-pillows-and-blankets]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/extra-pillows-and-blankets.svg?ref_type=heads "Extra pillows and blankets"
[airbnb-fire-extinguisher]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-extinguisher.svg?ref_type=heads "Fire extinguisher"
[airbnb-fire-pit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-pit.svg?ref_type=heads "Fire pit"
[airbnb-fire-screen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/fire-screen.svg?ref_type=heads "Fire screen"
[airbnb-first-aid-kit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/first-aid-kit.svg?ref_type=heads "First aid kit"
[airbnb-free-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-parking-on-premises.svg?ref_type=heads "Free parking on premises"
[airbnb-free-street-parking]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/free-street-parking.svg?ref_type=heads "Free street parking"
[airbnb-freezer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/freezer.svg?ref_type=heads "Freezer"
[airbnb-game-console]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/game-console.svg?ref_type=heads "Game console"
[airbnb-gym]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/gym.svg?ref_type=heads "Gym"
[airbnb-hair-dryer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hair-dryer.svg?ref_type=heads "Hair dryer"
[airbnb-hammock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hammock.svg?ref_type=heads "Hammock"
[airbnb-hangers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hangers.svg?ref_type=heads "Hangers"
[airbnb-heating]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/heating.svg?ref_type=heads "Heating"
[airbnb-high-chair]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/high-chair.svg?ref_type=heads "High chair"
[airbnb-hockey-rink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hockey-rink.svg?ref_type=heads "Hockey rink"
[airbnb-hot-tub]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-tub.svg?ref_type=heads "Hot tub"
[airbnb-hot-water]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water.svg?ref_type=heads "Hot water"
[airbnb-hot-water-kettle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/hot-water-kettle.svg?ref_type=heads "Hot water kettle"
[airbnb-indoor-fireplace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/indoor-fireplace.svg?ref_type=heads "Indoor fireplace"
[airbnb-iron]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/iron.svg?ref_type=heads "Iron"
[airbnb-kayak]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kayak.svg?ref_type=heads "Kayak"
[airbnb-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchen.svg?ref_type=heads "Kitchen"
[airbnb-kitchenette]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/kitchenette.svg?ref_type=heads "Kitchenette"
[airbnb-lake-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/lake-access.svg?ref_type=heads "Lake access"
[airbnb-laser-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laser-tag.svg?ref_type=heads "Laser tag"
[airbnb-laundromat-nearby]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/laundromat-nearby.svg?ref_type=heads "Laundromat nearby"
[airbnb-life-size-games]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/life-size-games.svg?ref_type=heads "Life size games"
[airbnb-long-term-stays-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/long-term-stays-allowed.svg?ref_type=heads "Long term stays allowed"
[airbnb-luggage-dropoff-allowed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/luggage-dropoff-allowed.svg?ref_type=heads "Luggage dropoff allowed"
[airbnb-microwave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/microwave.svg?ref_type=heads "Microwave"
[airbnb-mini-fridge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-fridge.svg?ref_type=heads "Mini fridge"
[airbnb-mini-golf]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mini-golf.svg?ref_type=heads "Mini golf"
[airbnb-mosquito-net]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/mosquito-net.svg?ref_type=heads "Mosquito net"
[airbnb-movie-theater]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/movie-theater.svg?ref_type=heads "Movie theater"
[airbnb-outdoor-dining-area]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-dining-area.svg?ref_type=heads "Outdoor dining area"
[airbnb-outdoor-furniture]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-furniture.svg?ref_type=heads "Outdoor furniture"
[airbnb-outdoor-kitchen]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-kitchen.svg?ref_type=heads "Outdoor kitchen"
[airbnb-outdoor-playground]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-playground.svg?ref_type=heads "Outdoor playground"
[airbnb-outdoor-shower]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outdoor-shower.svg?ref_type=heads "Outdoor shower"
[airbnb-outlet-covers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/outlet-covers.svg?ref_type=heads "Outlet covers"
[airbnb-oven]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/oven.svg?ref_type=heads "Oven"
[airbnb-pack-n-play-travel-crib]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pack-n-play-travel-crib.svg?ref_type=heads "Pack ’n play/Travel crib"
[airbnb-paid-parking-off-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-off-premises.svg?ref_type=heads "Paid parking off premises"
[airbnb-paid-parking-on-premises]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/paid-parking-on-premises.svg?ref_type=heads "Paid parking on premises"
[airbnb-patio-or-balcony]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/patio-or-balcony.svg?ref_type=heads "Patio or balcony"
[airbnb-piano]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/piano.svg?ref_type=heads "Piano"
[airbnb-ping-pong-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ping-pong-table.svg?ref_type=heads "Ping pong table"
[airbnb-pocket-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pocket-wifi.svg?ref_type=heads "Pocket wifi"
[airbnb-pool]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool.svg?ref_type=heads "Pool"
[airbnb-pool-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/pool-table.svg?ref_type=heads "Pool table"
[airbnb-portable-fans]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/portable-fans.svg?ref_type=heads "Portable fans"
[airbnb-private-entrance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-entrance.svg?ref_type=heads "Private entrance"
[airbnb-private-living-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/private-living-room.svg?ref_type=heads "Private living room"
[airbnb-record-player]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/record-player.svg?ref_type=heads "Record player"
[airbnb-refrigerator]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/refrigerator.svg?ref_type=heads "Refrigerator"
[airbnb-resort-access]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/resort-access.svg?ref_type=heads "Resort access"
[airbnb-rice-maker]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/rice-maker.svg?ref_type=heads "Rice Maker"
[airbnb-room-darkening-shades]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/room-darkening-shades.svg?ref_type=heads "Room-darkening shades"
[airbnb-safe]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/safe.svg?ref_type=heads "Safe"
[airbnb-sauna]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sauna.svg?ref_type=heads "Sauna"
[airbnb-shampoo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shampoo.svg?ref_type=heads "Shampoo"
[airbnb-shower-gel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/shower-gel.svg?ref_type=heads "Shower gel"
[airbnb-single-level-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/single-level-home.svg?ref_type=heads "Single level home"
[airbnb-skate-ramp]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/skate-ramp.svg?ref_type=heads "Skate ramp"
[airbnb-ski-in-ski-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/ski-in-ski-out.svg?ref_type=heads "Ski-in/Ski-out"
[airbnb-smoke-alarm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/smoke-alarm.svg?ref_type=heads "Smoke alarm"
[airbnb-sound-system]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sound-system.svg?ref_type=heads "Sound system"
[airbnb-stove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/stove.svg?ref_type=heads "Stove"
[airbnb-sun-loungers]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/sun-loungers.svg?ref_type=heads "Sun loungers"
[airbnb-table-corner-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/table-corner-guards.svg?ref_type=heads "Table corner guards"
[airbnb-theme-room]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/theme-room.svg?ref_type=heads "Theme room"
[airbnb-toaster]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/toaster.svg?ref_type=heads "Toaster"
[airbnb-trash-compactor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/trash-compactor.svg?ref_type=heads "Trash compactor"
[airbnb-tv]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/tv.svg?ref_type=heads "TV"
[airbnb-washer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/washer.svg?ref_type=heads "Washer"
[airbnb-waterfront]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/waterfront.svg?ref_type=heads "Waterfront"
[airbnb-wifi]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wifi.svg?ref_type=heads "Wifi"
[airbnb-window-guards]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/window-guards.svg?ref_type=heads "Window guards"
[airbnb-wine-glasses]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/airbnb/wine-glasses.svg?ref_type=heads "Wine glasses"
