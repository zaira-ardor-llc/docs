<h1>Gitlab Icons</h1>

![Gitlab logo][gl-icon-tanuki]{height="100" width="100"}

> A common location of Gitlab icons to use in our docuementation.

<h2>Table of Contents</h2>

[TOC]

## Icons

There are 408 icons available.

1. <figure><figcaption><code>[gl-icon-abuse]</code></figcaption>

   ![abuse][gl-icon-abuse]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-accessibility]</code></figcaption>

   ![accessibility][gl-icon-accessibility]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-account]</code></figcaption>

   ![account][gl-icon-account]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-admin]</code></figcaption>

   ![admin][gl-icon-admin]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-api]</code></figcaption>

   ![api][gl-icon-api]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-appearance]</code></figcaption>

   ![appearance][gl-icon-appearance]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-applications]</code></figcaption>

   ![applications][gl-icon-applications]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-approval-solid]</code></figcaption>

   ![approval-solid][gl-icon-approval-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-approval]</code></figcaption>

   ![approval][gl-icon-approval]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-archive]</code></figcaption>

   ![archive][gl-icon-archive]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-arrow-down]</code></figcaption>

   ![arrow-down][gl-icon-arrow-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-arrow-left]</code></figcaption>

   ![arrow-left][gl-icon-arrow-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-arrow-right]</code></figcaption>

   ![arrow-right][gl-icon-arrow-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-arrow-up]</code></figcaption>

   ![arrow-up][gl-icon-arrow-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-assignee]</code></figcaption>

   ![assignee][gl-icon-assignee]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-at]</code></figcaption>

   ![at][gl-icon-at]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-attention-solid-sm]</code></figcaption>

   ![attention-solid-sm][gl-icon-attention-solid-sm]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-attention-solid]</code></figcaption>

   ![attention-solid][gl-icon-attention-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-attention]</code></figcaption>

   ![attention][gl-icon-attention]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-autoplay]</code></figcaption>

   ![autoplay][gl-icon-autoplay]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bitbucket]</code></figcaption>

   ![bitbucket][gl-icon-bitbucket]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bold]</code></figcaption>

   ![bold][gl-icon-bold]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-book]</code></figcaption>

   ![book][gl-icon-book]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bookmark]</code></figcaption>

   ![bookmark][gl-icon-bookmark]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-branch-deleted]</code></figcaption>

   ![branch-deleted][gl-icon-branch-deleted]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-branch]</code></figcaption>

   ![branch][gl-icon-branch]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bug]</code></figcaption>

   ![bug][gl-icon-bug]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-building]</code></figcaption>

   ![building][gl-icon-building]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bulb]</code></figcaption>

   ![bulb][gl-icon-bulb]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-bullhorn]</code></figcaption>

   ![bullhorn][gl-icon-bullhorn]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-calendar]</code></figcaption>

   ![calendar][gl-icon-calendar]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-cancel]</code></figcaption>

   ![cancel][gl-icon-cancel]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-canceled-circle]</code></figcaption>

   ![canceled-circle][gl-icon-canceled-circle]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-car]</code></figcaption>

   ![car][gl-icon-car]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-catalog-checkmark]</code></figcaption>

   ![catalog-checkmark][gl-icon-catalog-checkmark]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chart]</code></figcaption>

   ![chart][gl-icon-chart]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check-circle-dashed]</code></figcaption>

   ![check-circle-dashed][gl-icon-check-circle-dashed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check-circle-filled]</code></figcaption>

   ![check-circle-filled][gl-icon-check-circle-filled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check-circle]</code></figcaption>

   ![check-circle][gl-icon-check-circle]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check-sm]</code></figcaption>

   ![check-sm][gl-icon-check-sm]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check-xs]</code></figcaption>

   ![check-xs][gl-icon-check-xs]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-check]</code></figcaption>

   ![check][gl-icon-check]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-cherry-pick-commit]</code></figcaption>

   ![cherry-pick-commit][gl-icon-cherry-pick-commit]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-double-lg-left]</code></figcaption>

   ![chevron-double-lg-left][gl-icon-chevron-double-lg-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-double-lg-right]</code></figcaption>

   ![chevron-double-lg-right][gl-icon-chevron-double-lg-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-down]</code></figcaption>

   ![chevron-down][gl-icon-chevron-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-left]</code></figcaption>

   ![chevron-left][gl-icon-chevron-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-lg-down]</code></figcaption>

   ![chevron-lg-down][gl-icon-chevron-lg-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-lg-left]</code></figcaption>

   ![chevron-lg-left][gl-icon-chevron-lg-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-lg-right]</code></figcaption>

   ![chevron-lg-right][gl-icon-chevron-lg-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-lg-up]</code></figcaption>

   ![chevron-lg-up][gl-icon-chevron-lg-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-right]</code></figcaption>

   ![chevron-right][gl-icon-chevron-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-chevron-up]</code></figcaption>

   ![chevron-up][gl-icon-chevron-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-clear-all]</code></figcaption>

   ![clear-all][gl-icon-clear-all]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-clear]</code></figcaption>

   ![clear][gl-icon-clear]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-clock]</code></figcaption>

   ![clock][gl-icon-clock]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-close-xs]</code></figcaption>

   ![close-xs][gl-icon-close-xs]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-close]</code></figcaption>

   ![close][gl-icon-close]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-cloud-gear]</code></figcaption>

   ![cloud-gear][gl-icon-cloud-gear]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-cloud-pod]</code></figcaption>

   ![cloud-pod][gl-icon-cloud-pod]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-cloud-terminal]</code></figcaption>

   ![cloud-terminal][gl-icon-cloud-terminal]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-code]</code></figcaption>

   ![code][gl-icon-code]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-collapse-left]</code></figcaption>

   ![collapse-left][gl-icon-collapse-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-collapse-right]</code></figcaption>

   ![collapse-right][gl-icon-collapse-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-collapse-solid]</code></figcaption>

   ![collapse-solid][gl-icon-collapse-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-collapse]</code></figcaption>

   ![collapse][gl-icon-collapse]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comment-dots]</code></figcaption>

   ![comment-dots][gl-icon-comment-dots]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comment-lines]</code></figcaption>

   ![comment-lines][gl-icon-comment-lines]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comment-next]</code></figcaption>

   ![comment-next][gl-icon-comment-next]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comment]</code></figcaption>

   ![comment][gl-icon-comment]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comments]</code></figcaption>

   ![comments][gl-icon-comments]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-commit]</code></figcaption>

   ![commit][gl-icon-commit]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-comparison]</code></figcaption>

   ![comparison][gl-icon-comparison]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-compass]</code></figcaption>

   ![compass][gl-icon-compass]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-connected]</code></figcaption>

   ![connected][gl-icon-connected]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-container-image]</code></figcaption>

   ![container-image][gl-icon-container-image]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-copy-to-clipboard]</code></figcaption>

   ![copy-to-clipboard][gl-icon-copy-to-clipboard]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-credit-card]</code></figcaption>

   ![credit-card][gl-icon-credit-card]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dash-circle]</code></figcaption>

   ![dash-circle][gl-icon-dash-circle]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dash]</code></figcaption>

   ![dash][gl-icon-dash]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dashboard]</code></figcaption>

   ![dashboard][gl-icon-dashboard]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-deployments]</code></figcaption>

   ![deployments][gl-icon-deployments]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-details-block]</code></figcaption>

   ![details-block][gl-icon-details-block]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-diagram]</code></figcaption>

   ![diagram][gl-icon-diagram]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-discord]</code></figcaption>

   ![discord][gl-icon-discord]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-disk]</code></figcaption>

   ![disk][gl-icon-disk]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-changes]</code></figcaption>

   ![doc-changes][gl-icon-doc-changes]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-chart]</code></figcaption>

   ![doc-chart][gl-icon-doc-chart]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-code]</code></figcaption>

   ![doc-code][gl-icon-doc-code]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-compressed]</code></figcaption>

   ![doc-compressed][gl-icon-doc-compressed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-expand]</code></figcaption>

   ![doc-expand][gl-icon-doc-expand]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-image]</code></figcaption>

   ![doc-image][gl-icon-doc-image]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-new]</code></figcaption>

   ![doc-new][gl-icon-doc-new]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-symlink]</code></figcaption>

   ![doc-symlink][gl-icon-doc-symlink]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-text]</code></figcaption>

   ![doc-text][gl-icon-doc-text]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-doc-versions]</code></figcaption>

   ![doc-versions][gl-icon-doc-versions]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-document]</code></figcaption>

   ![document][gl-icon-document]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-documents]</code></figcaption>

   ![documents][gl-icon-documents]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dot-grid]</code></figcaption>

   ![dot-grid][gl-icon-dot-grid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dotted-circle]</code></figcaption>

   ![dotted-circle][gl-icon-dotted-circle]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-double-headed-arrow]</code></figcaption>

   ![double-headed-arrow][gl-icon-double-headed-arrow]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-download]</code></figcaption>

   ![download][gl-icon-download]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-dumbbell]</code></figcaption>

   ![dumbbell][gl-icon-dumbbell]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-duplicate]</code></figcaption>

   ![duplicate][gl-icon-duplicate]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-earth]</code></figcaption>

   ![earth][gl-icon-earth]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-ellipsis_h]</code></figcaption>

   ![ellipsis_h][gl-icon-ellipsis_h]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-ellipsis_v]</code></figcaption>

   ![ellipsis_v][gl-icon-ellipsis_v]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-entity-blocked]</code></figcaption>

   ![entity-blocked][gl-icon-entity-blocked]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-environment]</code></figcaption>

   ![environment][gl-icon-environment]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-epic-closed]</code></figcaption>

   ![epic-closed][gl-icon-epic-closed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-epic]</code></figcaption>

   ![epic][gl-icon-epic]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-error]</code></figcaption>

   ![error][gl-icon-error]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expand-down]</code></figcaption>

   ![expand-down][gl-icon-expand-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expand-left]</code></figcaption>

   ![expand-left][gl-icon-expand-left]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expand-right]</code></figcaption>

   ![expand-right][gl-icon-expand-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expand-up]</code></figcaption>

   ![expand-up][gl-icon-expand-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expand]</code></figcaption>

   ![expand][gl-icon-expand]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-expire]</code></figcaption>

   ![expire][gl-icon-expire]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-export]</code></figcaption>

   ![export][gl-icon-export]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-external-link]</code></figcaption>

   ![external-link][gl-icon-external-link]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-eye-slash]</code></figcaption>

   ![eye-slash][gl-icon-eye-slash]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-eye]</code></figcaption>

   ![eye][gl-icon-eye]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-face-neutral]</code></figcaption>

   ![face-neutral][gl-icon-face-neutral]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-face-unhappy]</code></figcaption>

   ![face-unhappy][gl-icon-face-unhappy]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-false-positive]</code></figcaption>

   ![false-positive][gl-icon-false-positive]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-feature-flag-disabled]</code></figcaption>

   ![feature-flag-disabled][gl-icon-feature-flag-disabled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-feature-flag]</code></figcaption>

   ![feature-flag][gl-icon-feature-flag]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-addition-solid]</code></figcaption>

   ![file-addition-solid][gl-icon-file-addition-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-addition]</code></figcaption>

   ![file-addition][gl-icon-file-addition]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-deletion-solid]</code></figcaption>

   ![file-deletion-solid][gl-icon-file-deletion-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-deletion]</code></figcaption>

   ![file-deletion][gl-icon-file-deletion]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-modified-solid]</code></figcaption>

   ![file-modified-solid][gl-icon-file-modified-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-modified]</code></figcaption>

   ![file-modified][gl-icon-file-modified]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-file-tree]</code></figcaption>

   ![file-tree][gl-icon-file-tree]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-filter]</code></figcaption>

   ![filter][gl-icon-filter]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-fire]</code></figcaption>

   ![fire][gl-icon-fire]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-first-contribution]</code></figcaption>

   ![first-contribution][gl-icon-first-contribution]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-flag]</code></figcaption>

   ![flag][gl-icon-flag]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-folder-new]</code></figcaption>

   ![folder-new][gl-icon-folder-new]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-folder-o]</code></figcaption>

   ![folder-o][gl-icon-folder-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-folder-open]</code></figcaption>

   ![folder-open][gl-icon-folder-open]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-folder]</code></figcaption>

   ![folder][gl-icon-folder]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-food]</code></figcaption>

   ![food][gl-icon-food]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-fork]</code></figcaption>

   ![fork][gl-icon-fork]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-formula]</code></figcaption>

   ![formula][gl-icon-formula]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-git-merge]</code></figcaption>

   ![git-merge][gl-icon-git-merge]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-git]</code></figcaption>

   ![git][gl-icon-git]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-gitea]</code></figcaption>

   ![gitea][gl-icon-gitea]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-github]</code></figcaption>

   ![github][gl-icon-github]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-go-back]</code></figcaption>

   ![go-back][gl-icon-go-back]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-google]</code></figcaption>

   ![google][gl-icon-google]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-grip]</code></figcaption>

   ![grip][gl-icon-grip]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-group]</code></figcaption>

   ![group][gl-icon-group]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-hamburger]</code></figcaption>

   ![hamburger][gl-icon-hamburger]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-heading]</code></figcaption>

   ![heading][gl-icon-heading]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-heart]</code></figcaption>

   ![heart][gl-icon-heart]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-highlight]</code></figcaption>

   ![highlight][gl-icon-highlight]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-history]</code></figcaption>

   ![history][gl-icon-history]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-home]</code></figcaption>

   ![home][gl-icon-home]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-hook]</code></figcaption>

   ![hook][gl-icon-hook]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-hourglass]</code></figcaption>

   ![hourglass][gl-icon-hourglass]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-image-comment-dark]</code></figcaption>

   ![image-comment-dark][gl-icon-image-comment-dark]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-image-comment-light]</code></figcaption>

   ![image-comment-light][gl-icon-image-comment-light]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-import]</code></figcaption>

   ![import][gl-icon-import]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-incognito]</code></figcaption>

   ![incognito][gl-icon-incognito]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-information-o]</code></figcaption>

   ![information-o][gl-icon-information-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-information]</code></figcaption>

   ![information][gl-icon-information]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-infrastructure-registry]</code></figcaption>

   ![infrastructure-registry][gl-icon-infrastructure-registry]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-block]</code></figcaption>

   ![issue-block][gl-icon-issue-block]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-close]</code></figcaption>

   ![issue-close][gl-icon-issue-close]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-closed]</code></figcaption>

   ![issue-closed][gl-icon-issue-closed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-new]</code></figcaption>

   ![issue-new][gl-icon-issue-new]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-open-m]</code></figcaption>

   ![issue-open-m][gl-icon-issue-open-m]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-enhancement]</code></figcaption>

   ![issue-type-enhancement][gl-icon-issue-type-enhancement]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-feature-flag]</code></figcaption>

   ![issue-type-feature-flag][gl-icon-issue-type-feature-flag]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-feature]</code></figcaption>

   ![issue-type-feature][gl-icon-issue-type-feature]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-incident]</code></figcaption>

   ![issue-type-incident][gl-icon-issue-type-incident]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-issue]</code></figcaption>

   ![issue-type-issue][gl-icon-issue-type-issue]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-keyresult]</code></figcaption>

   ![issue-type-keyresult][gl-icon-issue-type-keyresult]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-maintenance]</code></figcaption>

   ![issue-type-maintenance][gl-icon-issue-type-maintenance]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-objective]</code></figcaption>

   ![issue-type-objective][gl-icon-issue-type-objective]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-requirements]</code></figcaption>

   ![issue-type-requirements][gl-icon-issue-type-requirements]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-task]</code></figcaption>

   ![issue-type-task][gl-icon-issue-type-task]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-test-case]</code></figcaption>

   ![issue-type-test-case][gl-icon-issue-type-test-case]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-type-ticket]</code></figcaption>

   ![issue-type-ticket][gl-icon-issue-type-ticket]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issue-update]</code></figcaption>

   ![issue-update][gl-icon-issue-update]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-issues]</code></figcaption>

   ![issues][gl-icon-issues]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-italic]</code></figcaption>

   ![italic][gl-icon-italic]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-iteration]</code></figcaption>

   ![iteration][gl-icon-iteration]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-key]</code></figcaption>

   ![key][gl-icon-key]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-keyboard]</code></figcaption>

   ![keyboard][gl-icon-keyboard]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-kubernetes-agent]</code></figcaption>

   ![kubernetes-agent][gl-icon-kubernetes-agent]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-kubernetes]</code></figcaption>

   ![kubernetes][gl-icon-kubernetes]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-label]</code></figcaption>

   ![label][gl-icon-label]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-labels]</code></figcaption>

   ![labels][gl-icon-labels]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-leave]</code></figcaption>

   ![leave][gl-icon-leave]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-level-up]</code></figcaption>

   ![level-up][gl-icon-level-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-license-sm]</code></figcaption>

   ![license-sm][gl-icon-license-sm]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-license]</code></figcaption>

   ![license][gl-icon-license]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-link]</code></figcaption>

   ![link][gl-icon-link]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-linkedin]</code></figcaption>

   ![linkedin][gl-icon-linkedin]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-list-bulleted]</code></figcaption>

   ![list-bulleted][gl-icon-list-bulleted]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-list-indent]</code></figcaption>

   ![list-indent][gl-icon-list-indent]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-list-numbered]</code></figcaption>

   ![list-numbered][gl-icon-list-numbered]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-list-outdent]</code></figcaption>

   ![list-outdent][gl-icon-list-outdent]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-list-task]</code></figcaption>

   ![list-task][gl-icon-list-task]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-live-preview]</code></figcaption>

   ![live-preview][gl-icon-live-preview]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-live-stream]</code></figcaption>

   ![live-stream][gl-icon-live-stream]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-location-dot]</code></figcaption>

   ![location-dot][gl-icon-location-dot]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-location]</code></figcaption>

   ![location][gl-icon-location]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-lock-open]</code></figcaption>

   ![lock-open][gl-icon-lock-open]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-lock]</code></figcaption>

   ![lock][gl-icon-lock]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-log]</code></figcaption>

   ![log][gl-icon-log]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-long-arrow]</code></figcaption>

   ![long-arrow][gl-icon-long-arrow]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-machine-learning]</code></figcaption>

   ![machine-learning][gl-icon-machine-learning]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-mail]</code></figcaption>

   ![mail][gl-icon-mail]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-markdown-mark-solid]</code></figcaption>

   ![markdown-mark-solid][gl-icon-markdown-mark-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-markdown-mark]</code></figcaption>

   ![markdown-mark][gl-icon-markdown-mark]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-marquee-selection]</code></figcaption>

   ![marquee-selection][gl-icon-marquee-selection]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-mastodon]</code></figcaption>

   ![mastodon][gl-icon-mastodon]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-maximize]</code></figcaption>

   ![maximize][gl-icon-maximize]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-media-broken]</code></figcaption>

   ![media-broken][gl-icon-media-broken]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-media]</code></figcaption>

   ![media][gl-icon-media]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-merge-request-close-m]</code></figcaption>

   ![merge-request-close-m][gl-icon-merge-request-close-m]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-merge-request-close]</code></figcaption>

   ![merge-request-close][gl-icon-merge-request-close]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-merge-request-open]</code></figcaption>

   ![merge-request-open][gl-icon-merge-request-open]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-merge-request]</code></figcaption>

   ![merge-request][gl-icon-merge-request]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-merge]</code></figcaption>

   ![merge][gl-icon-merge]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-messages]</code></figcaption>

   ![messages][gl-icon-messages]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-milestone]</code></figcaption>

   ![milestone][gl-icon-milestone]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-minimize]</code></figcaption>

   ![minimize][gl-icon-minimize]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-mobile-issue-close]</code></figcaption>

   ![mobile-issue-close][gl-icon-mobile-issue-close]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-mobile]</code></figcaption>

   ![mobile][gl-icon-mobile]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-monitor-lines]</code></figcaption>

   ![monitor-lines][gl-icon-monitor-lines]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-monitor-o]</code></figcaption>

   ![monitor-o][gl-icon-monitor-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-monitor]</code></figcaption>

   ![monitor][gl-icon-monitor]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-namespace]</code></figcaption>

   ![namespace][gl-icon-namespace]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-nature]</code></figcaption>

   ![nature][gl-icon-nature]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-notifications-off]</code></figcaption>

   ![notifications-off][gl-icon-notifications-off]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-notifications]</code></figcaption>

   ![notifications][gl-icon-notifications]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-object]</code></figcaption>

   ![object][gl-icon-object]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-organization]</code></figcaption>

   ![organization][gl-icon-organization]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-overview]</code></figcaption>

   ![overview][gl-icon-overview]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-package]</code></figcaption>

   ![package][gl-icon-package]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-paper-airplane]</code></figcaption>

   ![paper-airplane][gl-icon-paper-airplane]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-paperclip]</code></figcaption>

   ![paperclip][gl-icon-paperclip]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-partner-verified]</code></figcaption>

   ![partner-verified][gl-icon-partner-verified]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-pause]</code></figcaption>

   ![pause][gl-icon-pause]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-pencil-square]</code></figcaption>

   ![pencil-square][gl-icon-pencil-square]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-pencil]</code></figcaption>

   ![pencil][gl-icon-pencil]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-pipeline]</code></figcaption>

   ![pipeline][gl-icon-pipeline]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-planning]</code></figcaption>

   ![planning][gl-icon-planning]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-play]</code></figcaption>

   ![play][gl-icon-play]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-plus-square-o]</code></figcaption>

   ![plus-square-o][gl-icon-plus-square-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-plus-square]</code></figcaption>

   ![plus-square][gl-icon-plus-square]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-plus]</code></figcaption>

   ![plus][gl-icon-plus]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-pod]</code></figcaption>

   ![pod][gl-icon-pod]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-podcast]</code></figcaption>

   ![podcast][gl-icon-podcast]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-power]</code></figcaption>

   ![power][gl-icon-power]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-preferences]</code></figcaption>

   ![preferences][gl-icon-preferences]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-profile]</code></figcaption>

   ![profile][gl-icon-profile]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-progress]</code></figcaption>

   ![progress][gl-icon-progress]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-project]</code></figcaption>

   ![project][gl-icon-project]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-push-rules]</code></figcaption>

   ![push-rules][gl-icon-push-rules]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-question-o]</code></figcaption>

   ![question-o][gl-icon-question-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-question]</code></figcaption>

   ![question][gl-icon-question]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-quick-actions]</code></figcaption>

   ![quick-actions][gl-icon-quick-actions]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-quota]</code></figcaption>

   ![quota][gl-icon-quota]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-quote]</code></figcaption>

   ![quote][gl-icon-quote]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-redo]</code></figcaption>

   ![redo][gl-icon-redo]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-regular-expression]</code></figcaption>

   ![regular-expression][gl-icon-regular-expression]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-remove-all]</code></figcaption>

   ![remove-all][gl-icon-remove-all]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-remove]</code></figcaption>

   ![remove][gl-icon-remove]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-repeat]</code></figcaption>

   ![repeat][gl-icon-repeat]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-reply]</code></figcaption>

   ![reply][gl-icon-reply]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-requirements]</code></figcaption>

   ![requirements][gl-icon-requirements]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-retry]</code></figcaption>

   ![retry][gl-icon-retry]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-review-checkmark]</code></figcaption>

   ![review-checkmark][gl-icon-review-checkmark]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-review-list]</code></figcaption>

   ![review-list][gl-icon-review-list]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-review-warning]</code></figcaption>

   ![review-warning][gl-icon-review-warning]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-rocket-launch]</code></figcaption>

   ![rocket-launch][gl-icon-rocket-launch]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-rocket]</code></figcaption>

   ![rocket][gl-icon-rocket]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-rss]</code></figcaption>

   ![rss][gl-icon-rss]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-scale]</code></figcaption>

   ![scale][gl-icon-scale]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-scroll-handle]</code></figcaption>

   ![scroll-handle][gl-icon-scroll-handle]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-scroll_down]</code></figcaption>

   ![scroll_down][gl-icon-scroll_down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-scroll_up]</code></figcaption>

   ![scroll_up][gl-icon-scroll_up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search-dot]</code></figcaption>

   ![search-dot][gl-icon-search-dot]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search-minus]</code></figcaption>

   ![search-minus][gl-icon-search-minus]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search-plus]</code></figcaption>

   ![search-plus][gl-icon-search-plus]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search-results]</code></figcaption>

   ![search-results][gl-icon-search-results]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search-sm]</code></figcaption>

   ![search-sm][gl-icon-search-sm]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-search]</code></figcaption>

   ![search][gl-icon-search]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-settings]</code></figcaption>

   ![settings][gl-icon-settings]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-critical]</code></figcaption>

   ![severity-critical][gl-icon-severity-critical]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-high]</code></figcaption>

   ![severity-high][gl-icon-severity-high]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-info]</code></figcaption>

   ![severity-info][gl-icon-severity-info]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-low]</code></figcaption>

   ![severity-low][gl-icon-severity-low]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-medium]</code></figcaption>

   ![severity-medium][gl-icon-severity-medium]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-severity-unknown]</code></figcaption>

   ![severity-unknown][gl-icon-severity-unknown]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-share]</code></figcaption>

   ![share][gl-icon-share]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-shield]</code></figcaption>

   ![shield][gl-icon-shield]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-sidebar-right]</code></figcaption>

   ![sidebar-right][gl-icon-sidebar-right]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-sidebar]</code></figcaption>

   ![sidebar][gl-icon-sidebar]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-skype]</code></figcaption>

   ![skype][gl-icon-skype]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-slight-frown]</code></figcaption>

   ![slight-frown][gl-icon-slight-frown]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-slight-smile]</code></figcaption>

   ![slight-smile][gl-icon-slight-smile]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-smart-card]</code></figcaption>

   ![smart-card][gl-icon-smart-card]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-smile]</code></figcaption>

   ![smile][gl-icon-smile]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-smiley]</code></figcaption>

   ![smiley][gl-icon-smiley]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-snippet]</code></figcaption>

   ![snippet][gl-icon-snippet]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-soft-unwrap]</code></figcaption>

   ![soft-unwrap][gl-icon-soft-unwrap]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-soft-wrap]</code></figcaption>

   ![soft-wrap][gl-icon-soft-wrap]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-sort-highest]</code></figcaption>

   ![sort-highest][gl-icon-sort-highest]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-sort-lowest]</code></figcaption>

   ![sort-lowest][gl-icon-sort-lowest]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-spam]</code></figcaption>

   ![spam][gl-icon-spam]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-spinner]</code></figcaption>

   ![spinner][gl-icon-spinner]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-stage-all]</code></figcaption>

   ![stage-all][gl-icon-stage-all]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-star-o]</code></figcaption>

   ![star-o][gl-icon-star-o]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-star]</code></figcaption>

   ![star][gl-icon-star]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-active]</code></figcaption>

   ![status-active][gl-icon-status-active]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-alert]</code></figcaption>

   ![status-alert][gl-icon-status-alert]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-cancelled]</code></figcaption>

   ![status-cancelled][gl-icon-status-cancelled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-failed]</code></figcaption>

   ![status-failed][gl-icon-status-failed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-health]</code></figcaption>

   ![status-health][gl-icon-status-health]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-neutral]</code></figcaption>

   ![status-neutral][gl-icon-status-neutral]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-paused]</code></figcaption>

   ![status-paused][gl-icon-status-paused]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-running]</code></figcaption>

   ![status-running][gl-icon-status-running]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-scheduled]</code></figcaption>

   ![status-scheduled][gl-icon-status-scheduled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-stopped]</code></figcaption>

   ![status-stopped][gl-icon-status-stopped]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-success]</code></figcaption>

   ![status-success][gl-icon-status-success]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status-waiting]</code></figcaption>

   ![status-waiting][gl-icon-status-waiting]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status]</code></figcaption>

   ![status][gl-icon-status]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_canceled]</code></figcaption>

   ![status_canceled][gl-icon-status_canceled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_canceled_borderless]</code></figcaption>

   ![status_canceled_borderless][gl-icon-status_canceled_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_closed]</code></figcaption>

   ![status_closed][gl-icon-status_closed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_created]</code></figcaption>

   ![status_created][gl-icon-status_created]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_created_borderless]</code></figcaption>

   ![status_created_borderless][gl-icon-status_created_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_failed]</code></figcaption>

   ![status_failed][gl-icon-status_failed]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_failed_borderless]</code></figcaption>

   ![status_failed_borderless][gl-icon-status_failed_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_manual]</code></figcaption>

   ![status_manual][gl-icon-status_manual]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_manual_borderless]</code></figcaption>

   ![status_manual_borderless][gl-icon-status_manual_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_notfound]</code></figcaption>

   ![status_notfound][gl-icon-status_notfound]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_notfound_borderless]</code></figcaption>

   ![status_notfound_borderless][gl-icon-status_notfound_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_open]</code></figcaption>

   ![status_open][gl-icon-status_open]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_pending]</code></figcaption>

   ![status_pending][gl-icon-status_pending]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_pending_borderless]</code></figcaption>

   ![status_pending_borderless][gl-icon-status_pending_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_preparing]</code></figcaption>

   ![status_preparing][gl-icon-status_preparing]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_preparing_borderless]</code></figcaption>

   ![status_preparing_borderless][gl-icon-status_preparing_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_running]</code></figcaption>

   ![status_running][gl-icon-status_running]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_running_borderless]</code></figcaption>

   ![status_running_borderless][gl-icon-status_running_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_scheduled]</code></figcaption>

   ![status_scheduled][gl-icon-status_scheduled]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_scheduled_borderless]</code></figcaption>

   ![status_scheduled_borderless][gl-icon-status_scheduled_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_skipped]</code></figcaption>

   ![status_skipped][gl-icon-status_skipped]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_skipped_borderless]</code></figcaption>

   ![status_skipped_borderless][gl-icon-status_skipped_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_success]</code></figcaption>

   ![status_success][gl-icon-status_success]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_success_borderless]</code></figcaption>

   ![status_success_borderless][gl-icon-status_success_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_success_solid]</code></figcaption>

   ![status_success_solid][gl-icon-status_success_solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_warning]</code></figcaption>

   ![status_warning][gl-icon-status_warning]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-status_warning_borderless]</code></figcaption>

   ![status_warning_borderless][gl-icon-status_warning_borderless]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-stop]</code></figcaption>

   ![stop][gl-icon-stop]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-strikethrough]</code></figcaption>

   ![strikethrough][gl-icon-strikethrough]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-subgroup]</code></figcaption>

   ![subgroup][gl-icon-subgroup]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-subscript]</code></figcaption>

   ![subscript][gl-icon-subscript]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-substitute]</code></figcaption>

   ![substitute][gl-icon-substitute]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-superscript]</code></figcaption>

   ![superscript][gl-icon-superscript]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-symlink]</code></figcaption>

   ![symlink][gl-icon-symlink]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-table]</code></figcaption>

   ![table][gl-icon-table]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tablet]</code></figcaption>

   ![tablet][gl-icon-tablet]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tachometer]</code></figcaption>

   ![tachometer][gl-icon-tachometer]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tag]</code></figcaption>

   ![tag][gl-icon-tag]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tanuki-ai]</code></figcaption>

   ![tanuki-ai][gl-icon-tanuki-ai]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tanuki-verified]</code></figcaption>

   ![tanuki-verified][gl-icon-tanuki-verified]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-tanuki]</code></figcaption>

   ![tanuki][gl-icon-tanuki]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-task-done]</code></figcaption>

   ![task-done][gl-icon-task-done]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-template]</code></figcaption>

   ![template][gl-icon-template]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-terminal]</code></figcaption>

   ![terminal][gl-icon-terminal]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-terraform]</code></figcaption>

   ![terraform][gl-icon-terraform]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-text-description]</code></figcaption>

   ![text-description][gl-icon-text-description]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-thumb-down]</code></figcaption>

   ![thumb-down][gl-icon-thumb-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-thumb-up]</code></figcaption>

   ![thumb-up][gl-icon-thumb-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-thumbtack-solid]</code></figcaption>

   ![thumbtack-solid][gl-icon-thumbtack-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-thumbtack]</code></figcaption>

   ![thumbtack][gl-icon-thumbtack]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-time-out]</code></figcaption>

   ![time-out][gl-icon-time-out]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-timer]</code></figcaption>

   ![timer][gl-icon-timer]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-title]</code></figcaption>

   ![title][gl-icon-title]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-todo-add]</code></figcaption>

   ![todo-add][gl-icon-todo-add]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-todo-done]</code></figcaption>

   ![todo-done][gl-icon-todo-done]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-token]</code></figcaption>

   ![token][gl-icon-token]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-trend-down]</code></figcaption>

   ![trend-down][gl-icon-trend-down]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-trend-static]</code></figcaption>

   ![trend-static][gl-icon-trend-static]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-trend-up]</code></figcaption>

   ![trend-up][gl-icon-trend-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-trigger-source]</code></figcaption>

   ![trigger-source][gl-icon-trigger-source]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-unapproval]</code></figcaption>

   ![unapproval][gl-icon-unapproval]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-unassignee]</code></figcaption>

   ![unassignee][gl-icon-unassignee]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-underline]</code></figcaption>

   ![underline][gl-icon-underline]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-unlink]</code></figcaption>

   ![unlink][gl-icon-unlink]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-unstage-all]</code></figcaption>

   ![unstage-all][gl-icon-unstage-all]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-upgrade]</code></figcaption>

   ![upgrade][gl-icon-upgrade]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-upload]</code></figcaption>

   ![upload][gl-icon-upload]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-user]</code></figcaption>

   ![user][gl-icon-user]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-users]</code></figcaption>

   ![users][gl-icon-users]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-volume-up]</code></figcaption>

   ![volume-up][gl-icon-volume-up]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-warning-solid]</code></figcaption>

   ![warning-solid][gl-icon-warning-solid]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-warning]</code></figcaption>

   ![warning][gl-icon-warning]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-weight]</code></figcaption>

   ![weight][gl-icon-weight]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-work]</code></figcaption>

   ![work][gl-icon-work]{height="24" width="24"}

   </figure>

1. <figure><figcaption><code>[gl-icon-x]</code></figcaption>

   ![x][gl-icon-x]{height="24" width="24"}

   </figure>

## Usage

### How to display an icon

To use any of these icons, use the URL path

https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/{ICON_NAME}?ref_type=heads

replacing `{ICON_NAME}` with the file name of the icon you want to use.

+ _Example:_

    >>>
    Display the "commit" icon.

    commit.svg

    ```bash
    https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg?ref_type=heads
    
    ![Commit icon](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg?ref_type=heads)
    ```
    >>>

### How to resize an icon with Markdown

Use `{ height="\d+" width="\d+" }`

where `\d+` is a number with one or more digits.

+ _Examples:_

    >>>
    Display the "diagram" icon as 100 pixels high and 100 pixels wide.

    ```markdown
    ![Diagram](https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads){ height="100" width="100" }
    ```
    >>>

    or use its reference:

    >>>
    ```markdown
    ![Diagram][gl-icon-diagram]{height="100" width="100"}
    ```
    >>>

+ _Renders:_

    ![Diagram][gl-icon-diagram]{height="100" width="100"}

### How to resize an HTMLImageElement

To resize an [HTMLImageElement](https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement), 
set the `<img>`'s `height` and `width` attributes.

+ _Example:_

   >>>
   ```html
   <img alt="Diagram" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads" 
        height="50" 
        width="50">
    ```
    >>>

+ _Renders:_

   <img alt="Diagram" 
        src="https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads" 
        height="50" 
        width="50">

## Icon references

Copy and paste these image reference definitions into your Gitlab markdown documents.

```
[gl-icon-abuse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/abuse.svg?ref_type=heads
[gl-icon-accessibility]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/accessibility.svg?ref_type=heads
[gl-icon-account]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/account.svg?ref_type=heads
[gl-icon-admin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/admin.svg?ref_type=heads
[gl-icon-api]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads
[gl-icon-appearance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/appearance.svg?ref_type=heads
[gl-icon-applications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/applications.svg?ref_type=heads
[gl-icon-approval-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval-solid.svg?ref_type=heads
[gl-icon-approval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval.svg?ref_type=heads
[gl-icon-archive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/archive.svg?ref_type=heads
[gl-icon-arrow-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-down.svg?ref_type=heads
[gl-icon-arrow-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-left.svg?ref_type=heads
[gl-icon-arrow-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-right.svg?ref_type=heads
[gl-icon-arrow-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-up.svg?ref_type=heads
[gl-icon-assignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/assignee.svg?ref_type=heads
[gl-icon-at]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/at.svg?ref_type=heads
[gl-icon-attention-solid-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid-sm.svg?ref_type=heads
[gl-icon-attention-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid.svg?ref_type=heads
[gl-icon-attention]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention.svg?ref_type=heads
[gl-icon-autoplay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/autoplay.svg?ref_type=heads
[gl-icon-bitbucket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bitbucket.svg?ref_type=heads
[gl-icon-bold]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bold.svg?ref_type=heads
[gl-icon-book]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/book.svg?ref_type=heads
[gl-icon-bookmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bookmark.svg?ref_type=heads
[gl-icon-branch-deleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch-deleted.svg?ref_type=heads
[gl-icon-branch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch.svg?ref_type=heads
[gl-icon-brand-zoom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/brand-zoom.svg?ref_type=heads
[gl-icon-bug]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bug.svg?ref_type=heads
[gl-icon-building]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/building.svg?ref_type=heads
[gl-icon-bulb]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bulb.svg?ref_type=heads
[gl-icon-bullhorn]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bullhorn.svg?ref_type=heads
[gl-icon-calendar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/calendar.svg?ref_type=heads
[gl-icon-cancel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cancel.svg?ref_type=heads
[gl-icon-canceled-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/canceled-circle.svg?ref_type=heads
[gl-icon-car]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/car.svg?ref_type=heads
[gl-icon-catalog-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/catalog-checkmark.svg?ref_type=heads
[gl-icon-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chart.svg?ref_type=heads
[gl-icon-check-circle-dashed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-dashed.svg?ref_type=heads
[gl-icon-check-circle-filled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-filled.svg?ref_type=heads
[gl-icon-check-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle.svg?ref_type=heads
[gl-icon-check-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-sm.svg?ref_type=heads
[gl-icon-check-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-xs.svg?ref_type=heads
[gl-icon-check]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check.svg?ref_type=heads
[gl-icon-cherry-pick-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cherry-pick-commit.svg?ref_type=heads
[gl-icon-chevron-double-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-left.svg?ref_type=heads
[gl-icon-chevron-double-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-right.svg?ref_type=heads
[gl-icon-chevron-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-down.svg?ref_type=heads
[gl-icon-chevron-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-left.svg?ref_type=heads
[gl-icon-chevron-lg-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-down.svg?ref_type=heads
[gl-icon-chevron-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-left.svg?ref_type=heads
[gl-icon-chevron-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-right.svg?ref_type=heads
[gl-icon-chevron-lg-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-up.svg?ref_type=heads
[gl-icon-chevron-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-right.svg?ref_type=heads
[gl-icon-chevron-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-up.svg?ref_type=heads
[gl-icon-clear-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear-all.svg?ref_type=heads
[gl-icon-clear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear.svg?ref_type=heads
[gl-icon-clock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clock.svg?ref_type=heads
[gl-icon-close-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close-xs.svg?ref_type=heads
[gl-icon-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close.svg?ref_type=heads
[gl-icon-cloud-gear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-gear.svg?ref_type=heads
[gl-icon-cloud-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-pod.svg?ref_type=heads
[gl-icon-cloud-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-terminal.svg?ref_type=heads
[gl-icon-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/code.svg?ref_type=heads
[gl-icon-collapse-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-left.svg?ref_type=heads
[gl-icon-collapse-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-right.svg?ref_type=heads
[gl-icon-collapse-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-solid.svg?ref_type=heads
[gl-icon-collapse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse.svg?ref_type=heads
[gl-icon-comment-dots]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-dots.svg?ref_type=heads
[gl-icon-comment-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-lines.svg?ref_type=heads
[gl-icon-comment-next]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-next.svg?ref_type=heads
[gl-icon-comment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment.svg?ref_type=heads
[gl-icon-comments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comments.svg?ref_type=heads
[gl-icon-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg?ref_type=heads
[gl-icon-comparison]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comparison.svg?ref_type=heads
[gl-icon-compass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/compass.svg?ref_type=heads
[gl-icon-connected]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/connected.svg?ref_type=heads
[gl-icon-container-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/container-image.svg?ref_type=heads
[gl-icon-copy-to-clipboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/copy-to-clipboard.svg?ref_type=heads
[gl-icon-credit-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/credit-card.svg?ref_type=heads
[gl-icon-dash-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash-circle.svg?ref_type=heads
[gl-icon-dash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash.svg?ref_type=heads
[gl-icon-dashboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dashboard.svg?ref_type=heads
[gl-icon-deployments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/deployments.svg?ref_type=heads
[gl-icon-details-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/details-block.svg?ref_type=heads
[gl-icon-diagram]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads
[gl-icon-discord]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/discord.svg?ref_type=heads
[gl-icon-disk]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/disk.svg?ref_type=heads
[gl-icon-doc-changes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-changes.svg?ref_type=heads
[gl-icon-doc-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-chart.svg?ref_type=heads
[gl-icon-doc-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-code.svg?ref_type=heads
[gl-icon-doc-compressed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-compressed.svg?ref_type=heads
[gl-icon-doc-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-expand.svg?ref_type=heads
[gl-icon-doc-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-image.svg?ref_type=heads
[gl-icon-doc-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-new.svg?ref_type=heads
[gl-icon-doc-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-symlink.svg?ref_type=heads
[gl-icon-doc-text]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-text.svg?ref_type=heads
[gl-icon-doc-versions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-versions.svg?ref_type=heads
[gl-icon-document]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/document.svg?ref_type=heads
[gl-icon-documents]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/documents.svg?ref_type=heads
[gl-icon-dot-grid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dot-grid.svg?ref_type=heads
[gl-icon-dotted-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dotted-circle.svg?ref_type=heads
[gl-icon-double-headed-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/double-headed-arrow.svg?ref_type=heads
[gl-icon-download]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/download.svg?ref_type=heads
[gl-icon-dumbbell]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dumbbell.svg?ref_type=heads
[gl-icon-duplicate]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/duplicate.svg?ref_type=heads
[gl-icon-earth]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/earth.svg?ref_type=heads
[gl-icon-ellipsis_h]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_h.svg?ref_type=heads
[gl-icon-ellipsis_v]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_v.svg?ref_type=heads
[gl-icon-entity-blocked]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/entity-blocked.svg?ref_type=heads
[gl-icon-environment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/environment.svg?ref_type=heads
[gl-icon-epic-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic-closed.svg?ref_type=heads
[gl-icon-epic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic.svg?ref_type=heads
[gl-icon-error]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/error.svg?ref_type=heads
[gl-icon-expand-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-down.svg?ref_type=heads
[gl-icon-expand-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-left.svg?ref_type=heads
[gl-icon-expand-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-right.svg?ref_type=heads
[gl-icon-expand-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-up.svg?ref_type=heads
[gl-icon-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand.svg?ref_type=heads
[gl-icon-expire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expire.svg?ref_type=heads
[gl-icon-export]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/export.svg?ref_type=heads
[gl-icon-external-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/external-link.svg?ref_type=heads
[gl-icon-eye-slash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye-slash.svg?ref_type=heads
[gl-icon-eye]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye.svg?ref_type=heads
[gl-icon-face-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-neutral.svg?ref_type=heads
[gl-icon-face-unhappy]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-unhappy.svg?ref_type=heads
[gl-icon-false-positive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/false-positive.svg?ref_type=heads
[gl-icon-feature-flag-disabled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag-disabled.svg?ref_type=heads
[gl-icon-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag.svg?ref_type=heads
[gl-icon-file-addition-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition-solid.svg?ref_type=heads
[gl-icon-file-addition]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition.svg?ref_type=heads
[gl-icon-file-deletion-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion-solid.svg?ref_type=heads
[gl-icon-file-deletion]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion.svg?ref_type=heads
[gl-icon-file-modified-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified-solid.svg?ref_type=heads
[gl-icon-file-modified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified.svg?ref_type=heads
[gl-icon-file-tree]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-tree.svg?ref_type=heads
[gl-icon-filter]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/filter.svg?ref_type=heads
[gl-icon-fire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fire.svg?ref_type=heads
[gl-icon-first-contribution]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/first-contribution.svg?ref_type=heads
[gl-icon-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/flag.svg?ref_type=heads
[gl-icon-folder-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-new.svg?ref_type=heads
[gl-icon-folder-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-o.svg?ref_type=heads
[gl-icon-folder-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-open.svg?ref_type=heads
[gl-icon-folder]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder.svg?ref_type=heads
[gl-icon-food]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/food.svg?ref_type=heads
[gl-icon-fork]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fork.svg?ref_type=heads
[gl-icon-formula]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/formula.svg?ref_type=heads
[gl-icon-git-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git-merge.svg?ref_type=heads
[gl-icon-git]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git.svg?ref_type=heads
[gl-icon-gitea]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/gitea.svg?ref_type=heads
[gl-icon-github]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/github.svg?ref_type=heads
[gl-icon-go-back]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/go-back.svg?ref_type=heads
[gl-icon-google]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/google.svg?ref_type=heads
[gl-icon-grip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/grip.svg?ref_type=heads
[gl-icon-group]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/group.svg?ref_type=heads
[gl-icon-hamburger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hamburger.svg?ref_type=heads
[gl-icon-heading]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heading.svg?ref_type=heads
[gl-icon-heart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heart.svg?ref_type=heads
[gl-icon-highlight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/highlight.svg?ref_type=heads
[gl-icon-history]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/history.svg?ref_type=heads
[gl-icon-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/home.svg?ref_type=heads
[gl-icon-hook]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hook.svg?ref_type=heads
[gl-icon-hourglass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hourglass.svg?ref_type=heads
[gl-icon-image-comment-dark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-dark.svg?ref_type=heads
[gl-icon-image-comment-light]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-light.svg?ref_type=heads
[gl-icon-import]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/import.svg?ref_type=heads
[gl-icon-incognito]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/incognito.svg?ref_type=heads
[gl-icon-information-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information-o.svg?ref_type=heads
[gl-icon-information]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information.svg?ref_type=heads
[gl-icon-infrastructure-registry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/infrastructure-registry.svg?ref_type=heads
[gl-icon-issue-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-block.svg?ref_type=heads
[gl-icon-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-close.svg?ref_type=heads
[gl-icon-issue-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-closed.svg?ref_type=heads
[gl-icon-issue-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-new.svg?ref_type=heads
[gl-icon-issue-open-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-open-m.svg?ref_type=heads
[gl-icon-issue-type-enhancement]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-enhancement.svg?ref_type=heads
[gl-icon-issue-type-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature-flag.svg?ref_type=heads
[gl-icon-issue-type-feature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature.svg?ref_type=heads
[gl-icon-issue-type-incident]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-incident.svg?ref_type=heads
[gl-icon-issue-type-issue]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-issue.svg?ref_type=heads
[gl-icon-issue-type-keyresult]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-keyresult.svg?ref_type=heads
[gl-icon-issue-type-maintenance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-maintenance.svg?ref_type=heads
[gl-icon-issue-type-objective]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-objective.svg?ref_type=heads
[gl-icon-issue-type-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-requirements.svg?ref_type=heads
[gl-icon-issue-type-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-task.svg?ref_type=heads
[gl-icon-issue-type-test-case]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-test-case.svg?ref_type=heads
[gl-icon-issue-type-ticket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-ticket.svg?ref_type=heads
[gl-icon-issue-update]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-update.svg?ref_type=heads
[gl-icon-issues]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issues.svg?ref_type=heads
[gl-icon-italic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/italic.svg?ref_type=heads
[gl-icon-iteration]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/iteration.svg?ref_type=heads
[gl-icon-key]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/key.svg?ref_type=heads
[gl-icon-keyboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/keyboard.svg?ref_type=heads
[gl-icon-kubernetes-agent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes-agent.svg?ref_type=heads
[gl-icon-kubernetes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes.svg?ref_type=heads
[gl-icon-label]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/label.svg?ref_type=heads
[gl-icon-labels]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/labels.svg?ref_type=heads
[gl-icon-leave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/leave.svg?ref_type=heads
[gl-icon-level-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/level-up.svg?ref_type=heads
[gl-icon-license-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license-sm.svg?ref_type=heads
[gl-icon-license]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license.svg?ref_type=heads
[gl-icon-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/link.svg?ref_type=heads
[gl-icon-linkedin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/linkedin.svg?ref_type=heads
[gl-icon-list-bulleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-bulleted.svg?ref_type=heads
[gl-icon-list-indent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-indent.svg?ref_type=heads
[gl-icon-list-numbered]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-numbered.svg?ref_type=heads
[gl-icon-list-outdent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-outdent.svg?ref_type=heads
[gl-icon-list-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-task.svg?ref_type=heads
[gl-icon-live-preview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-preview.svg?ref_type=heads
[gl-icon-live-stream]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-stream.svg?ref_type=heads
[gl-icon-location-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location-dot.svg?ref_type=heads
[gl-icon-location]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location.svg?ref_type=heads
[gl-icon-lock-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock-open.svg?ref_type=heads
[gl-icon-lock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock.svg?ref_type=heads
[gl-icon-log]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/log.svg?ref_type=heads
[gl-icon-long-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/long-arrow.svg?ref_type=heads
[gl-icon-machine-learning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/machine-learning.svg?ref_type=heads
[gl-icon-mail]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mail.svg?ref_type=heads
[gl-icon-markdown-mark-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark-solid.svg?ref_type=heads
[gl-icon-markdown-mark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark.svg?ref_type=heads
[gl-icon-marquee-selection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/marquee-selection.svg?ref_type=heads
[gl-icon-mastodon]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mastodon.svg?ref_type=heads
[gl-icon-maximize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/maximize.svg?ref_type=heads
[gl-icon-media-broken]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media-broken.svg?ref_type=heads
[gl-icon-media]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media.svg?ref_type=heads
[gl-icon-merge-request-close-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close-m.svg?ref_type=heads
[gl-icon-merge-request-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close.svg?ref_type=heads
[gl-icon-merge-request-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-open.svg?ref_type=heads
[gl-icon-merge-request]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request.svg?ref_type=heads
[gl-icon-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge.svg?ref_type=heads
[gl-icon-messages]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/messages.svg?ref_type=heads
[gl-icon-milestone]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/milestone.svg?ref_type=heads
[gl-icon-minimize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/minimize.svg?ref_type=heads
[gl-icon-mobile-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile-issue-close.svg?ref_type=heads
[gl-icon-mobile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile.svg?ref_type=heads
[gl-icon-monitor-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-lines.svg?ref_type=heads
[gl-icon-monitor-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-o.svg?ref_type=heads
[gl-icon-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor.svg?ref_type=heads
[gl-icon-namespace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/namespace.svg?ref_type=heads
[gl-icon-nature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/nature.svg?ref_type=heads
[gl-icon-notifications-off]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications-off.svg?ref_type=heads
[gl-icon-notifications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications.svg?ref_type=heads
[gl-icon-object]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/object.svg?ref_type=heads
[gl-icon-organization]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/organization.svg?ref_type=heads
[gl-icon-overview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/overview.svg?ref_type=heads
[gl-icon-package]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/package.svg?ref_type=heads
[gl-icon-paper-airplane]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paper-airplane.svg?ref_type=heads
[gl-icon-paperclip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paperclip.svg?ref_type=heads
[gl-icon-partner-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/partner-verified.svg?ref_type=heads
[gl-icon-pause]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pause.svg?ref_type=heads
[gl-icon-pencil-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil-square.svg?ref_type=heads
[gl-icon-pencil]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil.svg?ref_type=heads
[gl-icon-pipeline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pipeline.svg?ref_type=heads
[gl-icon-planning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/planning.svg?ref_type=heads
[gl-icon-play]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/play.svg?ref_type=heads
[gl-icon-plus-square-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square-o.svg?ref_type=heads
[gl-icon-plus-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square.svg?ref_type=heads
[gl-icon-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus.svg?ref_type=heads
[gl-icon-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pod.svg?ref_type=heads
[gl-icon-podcast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/podcast.svg?ref_type=heads
[gl-icon-power]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/power.svg?ref_type=heads
[gl-icon-preferences]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/preferences.svg?ref_type=heads
[gl-icon-profile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/profile.svg?ref_type=heads
[gl-icon-progress]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/progress.svg?ref_type=heads
[gl-icon-project]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/project.svg?ref_type=heads
[gl-icon-push-rules]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/push-rules.svg?ref_type=heads
[gl-icon-question-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question-o.svg?ref_type=heads
[gl-icon-question]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question.svg?ref_type=heads
[gl-icon-quick-actions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quick-actions.svg?ref_type=heads
[gl-icon-quota]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quota.svg?ref_type=heads
[gl-icon-quote]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quote.svg?ref_type=heads
[gl-icon-redo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/redo.svg?ref_type=heads
[gl-icon-regular-expression]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/regular-expression.svg?ref_type=heads
[gl-icon-remove-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove-all.svg?ref_type=heads
[gl-icon-remove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove.svg?ref_type=heads
[gl-icon-repeat]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/repeat.svg?ref_type=heads
[gl-icon-reply]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/reply.svg?ref_type=heads
[gl-icon-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/requirements.svg?ref_type=heads
[gl-icon-retry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/retry.svg?ref_type=heads
[gl-icon-review-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-checkmark.svg?ref_type=heads
[gl-icon-review-list]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-list.svg?ref_type=heads
[gl-icon-review-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-warning.svg?ref_type=heads
[gl-icon-rocket-launch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket-launch.svg?ref_type=heads
[gl-icon-rocket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket.svg?ref_type=heads
[gl-icon-rss]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rss.svg?ref_type=heads
[gl-icon-scale]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scale.svg?ref_type=heads
[gl-icon-scroll-handle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll-handle.svg?ref_type=heads
[gl-icon-scroll_down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_down.svg?ref_type=heads
[gl-icon-scroll_up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_up.svg?ref_type=heads
[gl-icon-search-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-dot.svg?ref_type=heads
[gl-icon-search-minus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-minus.svg?ref_type=heads
[gl-icon-search-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-plus.svg?ref_type=heads
[gl-icon-search-results]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-results.svg?ref_type=heads
[gl-icon-search-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-sm.svg?ref_type=heads
[gl-icon-search]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search.svg?ref_type=heads
[gl-icon-settings]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/settings.svg?ref_type=heads
[gl-icon-severity-critical]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-critical.svg?ref_type=heads
[gl-icon-severity-high]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-high.svg?ref_type=heads
[gl-icon-severity-info]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-info.svg?ref_type=heads
[gl-icon-severity-low]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-low.svg?ref_type=heads
[gl-icon-severity-medium]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-medium.svg?ref_type=heads
[gl-icon-severity-unknown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-unknown.svg?ref_type=heads
[gl-icon-share]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/share.svg?ref_type=heads
[gl-icon-shield]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/shield.svg?ref_type=heads
[gl-icon-sidebar-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar-right.svg?ref_type=heads
[gl-icon-sidebar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar.svg?ref_type=heads
[gl-icon-skype]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/skype.svg?ref_type=heads
[gl-icon-slight-frown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-frown.svg?ref_type=heads
[gl-icon-slight-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-smile.svg?ref_type=heads
[gl-icon-smart-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smart-card.svg?ref_type=heads
[gl-icon-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smile.svg?ref_type=heads
[gl-icon-smiley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smiley.svg?ref_type=heads
[gl-icon-snippet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/snippet.svg?ref_type=heads
[gl-icon-soft-unwrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-unwrap.svg?ref_type=heads
[gl-icon-soft-wrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-wrap.svg?ref_type=heads
[gl-icon-sort-highest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-highest.svg?ref_type=heads
[gl-icon-sort-lowest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-lowest.svg?ref_type=heads
[gl-icon-spam]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spam.svg?ref_type=heads
[gl-icon-spinner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spinner.svg?ref_type=heads
[gl-icon-stage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stage-all.svg?ref_type=heads
[gl-icon-star-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star-o.svg?ref_type=heads
[gl-icon-star]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star.svg?ref_type=heads
[gl-icon-status-active]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-active.svg?ref_type=heads
[gl-icon-status-alert]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-alert.svg?ref_type=heads
[gl-icon-status-cancelled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-cancelled.svg?ref_type=heads
[gl-icon-status-failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-failed.svg?ref_type=heads
[gl-icon-status-health]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-health.svg?ref_type=heads
[gl-icon-status-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-neutral.svg?ref_type=heads
[gl-icon-status-paused]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-paused.svg?ref_type=heads
[gl-icon-status-running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-running.svg?ref_type=heads
[gl-icon-status-scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-scheduled.svg?ref_type=heads
[gl-icon-status-stopped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-stopped.svg?ref_type=heads
[gl-icon-status-success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-success.svg?ref_type=heads
[gl-icon-status-waiting]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-waiting.svg?ref_type=heads
[gl-icon-status]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status.svg?ref_type=heads
[gl-icon-status_canceled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled.svg?ref_type=heads
[gl-icon-status_canceled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled_borderless.svg?ref_type=heads
[gl-icon-status_closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_closed.svg?ref_type=heads
[gl-icon-status_created]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created.svg?ref_type=heads
[gl-icon-status_created_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created_borderless.svg?ref_type=heads
[gl-icon-status_failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed.svg?ref_type=heads
[gl-icon-status_failed_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed_borderless.svg?ref_type=heads
[gl-icon-status_manual]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual.svg?ref_type=heads
[gl-icon-status_manual_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual_borderless.svg?ref_type=heads
[gl-icon-status_notfound]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound.svg?ref_type=heads
[gl-icon-status_notfound_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound_borderless.svg?ref_type=heads
[gl-icon-status_open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_open.svg?ref_type=heads
[gl-icon-status_pending]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending.svg?ref_type=heads
[gl-icon-status_pending_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending_borderless.svg?ref_type=heads
[gl-icon-status_preparing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing.svg?ref_type=heads
[gl-icon-status_preparing_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing_borderless.svg?ref_type=heads
[gl-icon-status_running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running.svg?ref_type=heads
[gl-icon-status_running_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running_borderless.svg?ref_type=heads
[gl-icon-status_scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled.svg?ref_type=heads
[gl-icon-status_scheduled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled_borderless.svg?ref_type=heads
[gl-icon-status_skipped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped.svg?ref_type=heads
[gl-icon-status_skipped_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped_borderless.svg?ref_type=heads
[gl-icon-status_success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success.svg?ref_type=heads
[gl-icon-status_success_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_borderless.svg?ref_type=heads
[gl-icon-status_success_solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_solid.svg?ref_type=heads
[gl-icon-status_warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning.svg?ref_type=heads
[gl-icon-status_warning_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning_borderless.svg?ref_type=heads
[gl-icon-stop]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stop.svg?ref_type=heads
[gl-icon-strikethrough]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/strikethrough.svg?ref_type=heads
[gl-icon-subgroup]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subgroup.svg?ref_type=heads
[gl-icon-subscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subscript.svg?ref_type=heads
[gl-icon-substitute]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/substitute.svg?ref_type=heads
[gl-icon-superscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/superscript.svg?ref_type=heads
[gl-icon-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/symlink.svg?ref_type=heads
[gl-icon-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/table.svg?ref_type=heads
[gl-icon-tablet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tablet.svg?ref_type=heads
[gl-icon-tachometer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tachometer.svg?ref_type=heads
[gl-icon-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tag.svg?ref_type=heads
[gl-icon-tanuki-ai]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-ai.svg?ref_type=heads
[gl-icon-tanuki-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-verified.svg?ref_type=heads
[gl-icon-tanuki]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki.svg?ref_type=heads
[gl-icon-task-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/task-done.svg?ref_type=heads
[gl-icon-template]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/template.svg?ref_type=heads
[gl-icon-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terminal.svg?ref_type=heads
[gl-icon-terraform]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terraform.svg?ref_type=heads
[gl-icon-text-description]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/text-description.svg?ref_type=heads
[gl-icon-thumb-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-down.svg?ref_type=heads
[gl-icon-thumb-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-up.svg?ref_type=heads
[gl-icon-thumbtack-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack-solid.svg?ref_type=heads
[gl-icon-thumbtack]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack.svg?ref_type=heads
[gl-icon-time-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/time-out.svg?ref_type=heads
[gl-icon-timer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/timer.svg?ref_type=heads
[gl-icon-title]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/title.svg?ref_type=heads
[gl-icon-todo-add]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-add.svg?ref_type=heads
[gl-icon-todo-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-done.svg?ref_type=heads
[gl-icon-token]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/token.svg?ref_type=heads
[gl-icon-trend-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-down.svg?ref_type=heads
[gl-icon-trend-static]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-static.svg?ref_type=heads
[gl-icon-trend-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-up.svg?ref_type=heads
[gl-icon-trigger-source]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trigger-source.svg?ref_type=heads
[gl-icon-unapproval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unapproval.svg?ref_type=heads
[gl-icon-unassignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unassignee.svg?ref_type=heads
[gl-icon-underline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/underline.svg?ref_type=heads
[gl-icon-unlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unlink.svg?ref_type=heads
[gl-icon-unstage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unstage-all.svg?ref_type=heads
[gl-icon-upgrade]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upgrade.svg?ref_type=heads
[gl-icon-upload]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upload.svg?ref_type=heads
[gl-icon-user]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/user.svg?ref_type=heads
[gl-icon-users]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/users.svg?ref_type=heads
[gl-icon-volume-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/volume-up.svg?ref_type=heads
[gl-icon-warning-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning-solid.svg?ref_type=heads
[gl-icon-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning.svg?ref_type=heads
[gl-icon-weight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/weight.svg?ref_type=heads
[gl-icon-work]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/work.svg?ref_type=heads
[gl-icon-x]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/x.svg?ref_type=heads
```

[gl-icon-abuse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/abuse.svg?ref_type=heads
[gl-icon-accessibility]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/accessibility.svg?ref_type=heads
[gl-icon-account]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/account.svg?ref_type=heads
[gl-icon-admin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/admin.svg?ref_type=heads
[gl-icon-api]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/api.svg?ref_type=heads
[gl-icon-appearance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/appearance.svg?ref_type=heads
[gl-icon-applications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/applications.svg?ref_type=heads
[gl-icon-approval-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval-solid.svg?ref_type=heads
[gl-icon-approval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/approval.svg?ref_type=heads
[gl-icon-archive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/archive.svg?ref_type=heads
[gl-icon-arrow-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-down.svg?ref_type=heads
[gl-icon-arrow-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-left.svg?ref_type=heads
[gl-icon-arrow-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-right.svg?ref_type=heads
[gl-icon-arrow-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/arrow-up.svg?ref_type=heads
[gl-icon-assignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/assignee.svg?ref_type=heads
[gl-icon-at]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/at.svg?ref_type=heads
[gl-icon-attention-solid-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid-sm.svg?ref_type=heads
[gl-icon-attention-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention-solid.svg?ref_type=heads
[gl-icon-attention]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/attention.svg?ref_type=heads
[gl-icon-autoplay]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/autoplay.svg?ref_type=heads
[gl-icon-bitbucket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bitbucket.svg?ref_type=heads
[gl-icon-bold]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bold.svg?ref_type=heads
[gl-icon-book]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/book.svg?ref_type=heads
[gl-icon-bookmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bookmark.svg?ref_type=heads
[gl-icon-branch-deleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch-deleted.svg?ref_type=heads
[gl-icon-branch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/branch.svg?ref_type=heads
[gl-icon-brand-zoom]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/brand-zoom.svg?ref_type=heads
[gl-icon-bug]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bug.svg?ref_type=heads
[gl-icon-building]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/building.svg?ref_type=heads
[gl-icon-bulb]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bulb.svg?ref_type=heads
[gl-icon-bullhorn]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/bullhorn.svg?ref_type=heads
[gl-icon-calendar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/calendar.svg?ref_type=heads
[gl-icon-cancel]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cancel.svg?ref_type=heads
[gl-icon-canceled-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/canceled-circle.svg?ref_type=heads
[gl-icon-car]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/car.svg?ref_type=heads
[gl-icon-catalog-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/catalog-checkmark.svg?ref_type=heads
[gl-icon-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chart.svg?ref_type=heads
[gl-icon-check-circle-dashed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-dashed.svg?ref_type=heads
[gl-icon-check-circle-filled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle-filled.svg?ref_type=heads
[gl-icon-check-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-circle.svg?ref_type=heads
[gl-icon-check-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-sm.svg?ref_type=heads
[gl-icon-check-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check-xs.svg?ref_type=heads
[gl-icon-check]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/check.svg?ref_type=heads
[gl-icon-cherry-pick-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cherry-pick-commit.svg?ref_type=heads
[gl-icon-chevron-double-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-left.svg?ref_type=heads
[gl-icon-chevron-double-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-double-lg-right.svg?ref_type=heads
[gl-icon-chevron-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-down.svg?ref_type=heads
[gl-icon-chevron-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-left.svg?ref_type=heads
[gl-icon-chevron-lg-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-down.svg?ref_type=heads
[gl-icon-chevron-lg-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-left.svg?ref_type=heads
[gl-icon-chevron-lg-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-right.svg?ref_type=heads
[gl-icon-chevron-lg-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-lg-up.svg?ref_type=heads
[gl-icon-chevron-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-right.svg?ref_type=heads
[gl-icon-chevron-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/chevron-up.svg?ref_type=heads
[gl-icon-clear-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear-all.svg?ref_type=heads
[gl-icon-clear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clear.svg?ref_type=heads
[gl-icon-clock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/clock.svg?ref_type=heads
[gl-icon-close-xs]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close-xs.svg?ref_type=heads
[gl-icon-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/close.svg?ref_type=heads
[gl-icon-cloud-gear]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-gear.svg?ref_type=heads
[gl-icon-cloud-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-pod.svg?ref_type=heads
[gl-icon-cloud-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/cloud-terminal.svg?ref_type=heads
[gl-icon-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/code.svg?ref_type=heads
[gl-icon-collapse-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-left.svg?ref_type=heads
[gl-icon-collapse-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-right.svg?ref_type=heads
[gl-icon-collapse-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse-solid.svg?ref_type=heads
[gl-icon-collapse]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/collapse.svg?ref_type=heads
[gl-icon-comment-dots]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-dots.svg?ref_type=heads
[gl-icon-comment-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-lines.svg?ref_type=heads
[gl-icon-comment-next]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment-next.svg?ref_type=heads
[gl-icon-comment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comment.svg?ref_type=heads
[gl-icon-comments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comments.svg?ref_type=heads
[gl-icon-commit]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/commit.svg?ref_type=heads
[gl-icon-comparison]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/comparison.svg?ref_type=heads
[gl-icon-compass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/compass.svg?ref_type=heads
[gl-icon-connected]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/connected.svg?ref_type=heads
[gl-icon-container-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/container-image.svg?ref_type=heads
[gl-icon-copy-to-clipboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/copy-to-clipboard.svg?ref_type=heads
[gl-icon-credit-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/credit-card.svg?ref_type=heads
[gl-icon-dash-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash-circle.svg?ref_type=heads
[gl-icon-dash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dash.svg?ref_type=heads
[gl-icon-dashboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dashboard.svg?ref_type=heads
[gl-icon-deployments]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/deployments.svg?ref_type=heads
[gl-icon-details-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/details-block.svg?ref_type=heads
[gl-icon-diagram]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/diagram.svg?ref_type=heads
[gl-icon-discord]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/discord.svg?ref_type=heads
[gl-icon-disk]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/disk.svg?ref_type=heads
[gl-icon-doc-changes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-changes.svg?ref_type=heads
[gl-icon-doc-chart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-chart.svg?ref_type=heads
[gl-icon-doc-code]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-code.svg?ref_type=heads
[gl-icon-doc-compressed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-compressed.svg?ref_type=heads
[gl-icon-doc-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-expand.svg?ref_type=heads
[gl-icon-doc-image]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-image.svg?ref_type=heads
[gl-icon-doc-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-new.svg?ref_type=heads
[gl-icon-doc-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-symlink.svg?ref_type=heads
[gl-icon-doc-text]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-text.svg?ref_type=heads
[gl-icon-doc-versions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/doc-versions.svg?ref_type=heads
[gl-icon-document]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/document.svg?ref_type=heads
[gl-icon-documents]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/documents.svg?ref_type=heads
[gl-icon-dot-grid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dot-grid.svg?ref_type=heads
[gl-icon-dotted-circle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dotted-circle.svg?ref_type=heads
[gl-icon-double-headed-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/double-headed-arrow.svg?ref_type=heads
[gl-icon-download]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/download.svg?ref_type=heads
[gl-icon-dumbbell]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/dumbbell.svg?ref_type=heads
[gl-icon-duplicate]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/duplicate.svg?ref_type=heads
[gl-icon-earth]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/earth.svg?ref_type=heads
[gl-icon-ellipsis_h]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_h.svg?ref_type=heads
[gl-icon-ellipsis_v]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/ellipsis_v.svg?ref_type=heads
[gl-icon-entity-blocked]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/entity-blocked.svg?ref_type=heads
[gl-icon-environment]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/environment.svg?ref_type=heads
[gl-icon-epic-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic-closed.svg?ref_type=heads
[gl-icon-epic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/epic.svg?ref_type=heads
[gl-icon-error]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/error.svg?ref_type=heads
[gl-icon-expand-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-down.svg?ref_type=heads
[gl-icon-expand-left]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-left.svg?ref_type=heads
[gl-icon-expand-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-right.svg?ref_type=heads
[gl-icon-expand-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand-up.svg?ref_type=heads
[gl-icon-expand]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expand.svg?ref_type=heads
[gl-icon-expire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/expire.svg?ref_type=heads
[gl-icon-export]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/export.svg?ref_type=heads
[gl-icon-external-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/external-link.svg?ref_type=heads
[gl-icon-eye-slash]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye-slash.svg?ref_type=heads
[gl-icon-eye]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/eye.svg?ref_type=heads
[gl-icon-face-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-neutral.svg?ref_type=heads
[gl-icon-face-unhappy]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/face-unhappy.svg?ref_type=heads
[gl-icon-false-positive]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/false-positive.svg?ref_type=heads
[gl-icon-feature-flag-disabled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag-disabled.svg?ref_type=heads
[gl-icon-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/feature-flag.svg?ref_type=heads
[gl-icon-file-addition-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition-solid.svg?ref_type=heads
[gl-icon-file-addition]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-addition.svg?ref_type=heads
[gl-icon-file-deletion-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion-solid.svg?ref_type=heads
[gl-icon-file-deletion]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-deletion.svg?ref_type=heads
[gl-icon-file-modified-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified-solid.svg?ref_type=heads
[gl-icon-file-modified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-modified.svg?ref_type=heads
[gl-icon-file-tree]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/file-tree.svg?ref_type=heads
[gl-icon-filter]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/filter.svg?ref_type=heads
[gl-icon-fire]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fire.svg?ref_type=heads
[gl-icon-first-contribution]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/first-contribution.svg?ref_type=heads
[gl-icon-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/flag.svg?ref_type=heads
[gl-icon-folder-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-new.svg?ref_type=heads
[gl-icon-folder-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-o.svg?ref_type=heads
[gl-icon-folder-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder-open.svg?ref_type=heads
[gl-icon-folder]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/folder.svg?ref_type=heads
[gl-icon-food]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/food.svg?ref_type=heads
[gl-icon-fork]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/fork.svg?ref_type=heads
[gl-icon-formula]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/formula.svg?ref_type=heads
[gl-icon-git-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git-merge.svg?ref_type=heads
[gl-icon-git]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/git.svg?ref_type=heads
[gl-icon-gitea]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/gitea.svg?ref_type=heads
[gl-icon-github]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/github.svg?ref_type=heads
[gl-icon-go-back]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/go-back.svg?ref_type=heads
[gl-icon-google]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/google.svg?ref_type=heads
[gl-icon-grip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/grip.svg?ref_type=heads
[gl-icon-group]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/group.svg?ref_type=heads
[gl-icon-hamburger]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hamburger.svg?ref_type=heads
[gl-icon-heading]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heading.svg?ref_type=heads
[gl-icon-heart]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/heart.svg?ref_type=heads
[gl-icon-highlight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/highlight.svg?ref_type=heads
[gl-icon-history]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/history.svg?ref_type=heads
[gl-icon-home]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/home.svg?ref_type=heads
[gl-icon-hook]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hook.svg?ref_type=heads
[gl-icon-hourglass]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/hourglass.svg?ref_type=heads
[gl-icon-image-comment-dark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-dark.svg?ref_type=heads
[gl-icon-image-comment-light]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/image-comment-light.svg?ref_type=heads
[gl-icon-import]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/import.svg?ref_type=heads
[gl-icon-incognito]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/incognito.svg?ref_type=heads
[gl-icon-information-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information-o.svg?ref_type=heads
[gl-icon-information]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/information.svg?ref_type=heads
[gl-icon-infrastructure-registry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/infrastructure-registry.svg?ref_type=heads
[gl-icon-issue-block]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-block.svg?ref_type=heads
[gl-icon-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-close.svg?ref_type=heads
[gl-icon-issue-closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-closed.svg?ref_type=heads
[gl-icon-issue-new]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-new.svg?ref_type=heads
[gl-icon-issue-open-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-open-m.svg?ref_type=heads
[gl-icon-issue-type-enhancement]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-enhancement.svg?ref_type=heads
[gl-icon-issue-type-feature-flag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature-flag.svg?ref_type=heads
[gl-icon-issue-type-feature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-feature.svg?ref_type=heads
[gl-icon-issue-type-incident]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-incident.svg?ref_type=heads
[gl-icon-issue-type-issue]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-issue.svg?ref_type=heads
[gl-icon-issue-type-keyresult]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-keyresult.svg?ref_type=heads
[gl-icon-issue-type-maintenance]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-maintenance.svg?ref_type=heads
[gl-icon-issue-type-objective]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-objective.svg?ref_type=heads
[gl-icon-issue-type-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-requirements.svg?ref_type=heads
[gl-icon-issue-type-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-task.svg?ref_type=heads
[gl-icon-issue-type-test-case]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-test-case.svg?ref_type=heads
[gl-icon-issue-type-ticket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-type-ticket.svg?ref_type=heads
[gl-icon-issue-update]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issue-update.svg?ref_type=heads
[gl-icon-issues]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/issues.svg?ref_type=heads
[gl-icon-italic]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/italic.svg?ref_type=heads
[gl-icon-iteration]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/iteration.svg?ref_type=heads
[gl-icon-key]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/key.svg?ref_type=heads
[gl-icon-keyboard]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/keyboard.svg?ref_type=heads
[gl-icon-kubernetes-agent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes-agent.svg?ref_type=heads
[gl-icon-kubernetes]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/kubernetes.svg?ref_type=heads
[gl-icon-label]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/label.svg?ref_type=heads
[gl-icon-labels]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/labels.svg?ref_type=heads
[gl-icon-leave]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/leave.svg?ref_type=heads
[gl-icon-level-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/level-up.svg?ref_type=heads
[gl-icon-license-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license-sm.svg?ref_type=heads
[gl-icon-license]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/license.svg?ref_type=heads
[gl-icon-link]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/link.svg?ref_type=heads
[gl-icon-linkedin]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/linkedin.svg?ref_type=heads
[gl-icon-list-bulleted]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-bulleted.svg?ref_type=heads
[gl-icon-list-indent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-indent.svg?ref_type=heads
[gl-icon-list-numbered]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-numbered.svg?ref_type=heads
[gl-icon-list-outdent]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-outdent.svg?ref_type=heads
[gl-icon-list-task]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/list-task.svg?ref_type=heads
[gl-icon-live-preview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-preview.svg?ref_type=heads
[gl-icon-live-stream]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/live-stream.svg?ref_type=heads
[gl-icon-location-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location-dot.svg?ref_type=heads
[gl-icon-location]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/location.svg?ref_type=heads
[gl-icon-lock-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock-open.svg?ref_type=heads
[gl-icon-lock]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/lock.svg?ref_type=heads
[gl-icon-log]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/log.svg?ref_type=heads
[gl-icon-long-arrow]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/long-arrow.svg?ref_type=heads
[gl-icon-machine-learning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/machine-learning.svg?ref_type=heads
[gl-icon-mail]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mail.svg?ref_type=heads
[gl-icon-markdown-mark-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark-solid.svg?ref_type=heads
[gl-icon-markdown-mark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/markdown-mark.svg?ref_type=heads
[gl-icon-marquee-selection]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/marquee-selection.svg?ref_type=heads
[gl-icon-mastodon]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mastodon.svg?ref_type=heads
[gl-icon-maximize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/maximize.svg?ref_type=heads
[gl-icon-media-broken]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media-broken.svg?ref_type=heads
[gl-icon-media]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/media.svg?ref_type=heads
[gl-icon-merge-request-close-m]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close-m.svg?ref_type=heads
[gl-icon-merge-request-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-close.svg?ref_type=heads
[gl-icon-merge-request-open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request-open.svg?ref_type=heads
[gl-icon-merge-request]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge-request.svg?ref_type=heads
[gl-icon-merge]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/merge.svg?ref_type=heads
[gl-icon-messages]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/messages.svg?ref_type=heads
[gl-icon-milestone]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/milestone.svg?ref_type=heads
[gl-icon-minimize]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/minimize.svg?ref_type=heads
[gl-icon-mobile-issue-close]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile-issue-close.svg?ref_type=heads
[gl-icon-mobile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/mobile.svg?ref_type=heads
[gl-icon-monitor-lines]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-lines.svg?ref_type=heads
[gl-icon-monitor-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor-o.svg?ref_type=heads
[gl-icon-monitor]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/monitor.svg?ref_type=heads
[gl-icon-namespace]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/namespace.svg?ref_type=heads
[gl-icon-nature]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/nature.svg?ref_type=heads
[gl-icon-notifications-off]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications-off.svg?ref_type=heads
[gl-icon-notifications]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/notifications.svg?ref_type=heads
[gl-icon-object]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/object.svg?ref_type=heads
[gl-icon-organization]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/organization.svg?ref_type=heads
[gl-icon-overview]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/overview.svg?ref_type=heads
[gl-icon-package]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/package.svg?ref_type=heads
[gl-icon-paper-airplane]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paper-airplane.svg?ref_type=heads
[gl-icon-paperclip]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/paperclip.svg?ref_type=heads
[gl-icon-partner-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/partner-verified.svg?ref_type=heads
[gl-icon-pause]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pause.svg?ref_type=heads
[gl-icon-pencil-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil-square.svg?ref_type=heads
[gl-icon-pencil]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pencil.svg?ref_type=heads
[gl-icon-pipeline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pipeline.svg?ref_type=heads
[gl-icon-planning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/planning.svg?ref_type=heads
[gl-icon-play]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/play.svg?ref_type=heads
[gl-icon-plus-square-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square-o.svg?ref_type=heads
[gl-icon-plus-square]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus-square.svg?ref_type=heads
[gl-icon-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/plus.svg?ref_type=heads
[gl-icon-pod]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/pod.svg?ref_type=heads
[gl-icon-podcast]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/podcast.svg?ref_type=heads
[gl-icon-power]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/power.svg?ref_type=heads
[gl-icon-preferences]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/preferences.svg?ref_type=heads
[gl-icon-profile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/profile.svg?ref_type=heads
[gl-icon-progress]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/progress.svg?ref_type=heads
[gl-icon-project]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/project.svg?ref_type=heads
[gl-icon-push-rules]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/push-rules.svg?ref_type=heads
[gl-icon-question-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question-o.svg?ref_type=heads
[gl-icon-question]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/question.svg?ref_type=heads
[gl-icon-quick-actions]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quick-actions.svg?ref_type=heads
[gl-icon-quota]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quota.svg?ref_type=heads
[gl-icon-quote]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/quote.svg?ref_type=heads
[gl-icon-redo]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/redo.svg?ref_type=heads
[gl-icon-regular-expression]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/regular-expression.svg?ref_type=heads
[gl-icon-remove-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove-all.svg?ref_type=heads
[gl-icon-remove]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/remove.svg?ref_type=heads
[gl-icon-repeat]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/repeat.svg?ref_type=heads
[gl-icon-reply]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/reply.svg?ref_type=heads
[gl-icon-requirements]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/requirements.svg?ref_type=heads
[gl-icon-retry]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/retry.svg?ref_type=heads
[gl-icon-review-checkmark]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-checkmark.svg?ref_type=heads
[gl-icon-review-list]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-list.svg?ref_type=heads
[gl-icon-review-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/review-warning.svg?ref_type=heads
[gl-icon-rocket-launch]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket-launch.svg?ref_type=heads
[gl-icon-rocket]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rocket.svg?ref_type=heads
[gl-icon-rss]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/rss.svg?ref_type=heads
[gl-icon-scale]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scale.svg?ref_type=heads
[gl-icon-scroll-handle]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll-handle.svg?ref_type=heads
[gl-icon-scroll_down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_down.svg?ref_type=heads
[gl-icon-scroll_up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/scroll_up.svg?ref_type=heads
[gl-icon-search-dot]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-dot.svg?ref_type=heads
[gl-icon-search-minus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-minus.svg?ref_type=heads
[gl-icon-search-plus]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-plus.svg?ref_type=heads
[gl-icon-search-results]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-results.svg?ref_type=heads
[gl-icon-search-sm]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search-sm.svg?ref_type=heads
[gl-icon-search]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/search.svg?ref_type=heads
[gl-icon-settings]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/settings.svg?ref_type=heads
[gl-icon-severity-critical]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-critical.svg?ref_type=heads
[gl-icon-severity-high]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-high.svg?ref_type=heads
[gl-icon-severity-info]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-info.svg?ref_type=heads
[gl-icon-severity-low]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-low.svg?ref_type=heads
[gl-icon-severity-medium]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-medium.svg?ref_type=heads
[gl-icon-severity-unknown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/severity-unknown.svg?ref_type=heads
[gl-icon-share]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/share.svg?ref_type=heads
[gl-icon-shield]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/shield.svg?ref_type=heads
[gl-icon-sidebar-right]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar-right.svg?ref_type=heads
[gl-icon-sidebar]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sidebar.svg?ref_type=heads
[gl-icon-skype]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/skype.svg?ref_type=heads
[gl-icon-slight-frown]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-frown.svg?ref_type=heads
[gl-icon-slight-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/slight-smile.svg?ref_type=heads
[gl-icon-smart-card]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smart-card.svg?ref_type=heads
[gl-icon-smile]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smile.svg?ref_type=heads
[gl-icon-smiley]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/smiley.svg?ref_type=heads
[gl-icon-snippet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/snippet.svg?ref_type=heads
[gl-icon-soft-unwrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-unwrap.svg?ref_type=heads
[gl-icon-soft-wrap]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/soft-wrap.svg?ref_type=heads
[gl-icon-sort-highest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-highest.svg?ref_type=heads
[gl-icon-sort-lowest]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/sort-lowest.svg?ref_type=heads
[gl-icon-spam]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spam.svg?ref_type=heads
[gl-icon-spinner]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/spinner.svg?ref_type=heads
[gl-icon-stage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stage-all.svg?ref_type=heads
[gl-icon-star-o]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star-o.svg?ref_type=heads
[gl-icon-star]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/star.svg?ref_type=heads
[gl-icon-status-active]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-active.svg?ref_type=heads
[gl-icon-status-alert]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-alert.svg?ref_type=heads
[gl-icon-status-cancelled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-cancelled.svg?ref_type=heads
[gl-icon-status-failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-failed.svg?ref_type=heads
[gl-icon-status-health]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-health.svg?ref_type=heads
[gl-icon-status-neutral]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-neutral.svg?ref_type=heads
[gl-icon-status-paused]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-paused.svg?ref_type=heads
[gl-icon-status-running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-running.svg?ref_type=heads
[gl-icon-status-scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-scheduled.svg?ref_type=heads
[gl-icon-status-stopped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-stopped.svg?ref_type=heads
[gl-icon-status-success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-success.svg?ref_type=heads
[gl-icon-status-waiting]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status-waiting.svg?ref_type=heads
[gl-icon-status]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status.svg?ref_type=heads
[gl-icon-status_canceled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled.svg?ref_type=heads
[gl-icon-status_canceled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_canceled_borderless.svg?ref_type=heads
[gl-icon-status_closed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_closed.svg?ref_type=heads
[gl-icon-status_created]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created.svg?ref_type=heads
[gl-icon-status_created_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_created_borderless.svg?ref_type=heads
[gl-icon-status_failed]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed.svg?ref_type=heads
[gl-icon-status_failed_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_failed_borderless.svg?ref_type=heads
[gl-icon-status_manual]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual.svg?ref_type=heads
[gl-icon-status_manual_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_manual_borderless.svg?ref_type=heads
[gl-icon-status_notfound]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound.svg?ref_type=heads
[gl-icon-status_notfound_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_notfound_borderless.svg?ref_type=heads
[gl-icon-status_open]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_open.svg?ref_type=heads
[gl-icon-status_pending]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending.svg?ref_type=heads
[gl-icon-status_pending_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_pending_borderless.svg?ref_type=heads
[gl-icon-status_preparing]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing.svg?ref_type=heads
[gl-icon-status_preparing_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_preparing_borderless.svg?ref_type=heads
[gl-icon-status_running]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running.svg?ref_type=heads
[gl-icon-status_running_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_running_borderless.svg?ref_type=heads
[gl-icon-status_scheduled]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled.svg?ref_type=heads
[gl-icon-status_scheduled_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_scheduled_borderless.svg?ref_type=heads
[gl-icon-status_skipped]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped.svg?ref_type=heads
[gl-icon-status_skipped_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_skipped_borderless.svg?ref_type=heads
[gl-icon-status_success]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success.svg?ref_type=heads
[gl-icon-status_success_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_borderless.svg?ref_type=heads
[gl-icon-status_success_solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_success_solid.svg?ref_type=heads
[gl-icon-status_warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning.svg?ref_type=heads
[gl-icon-status_warning_borderless]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/status_warning_borderless.svg?ref_type=heads
[gl-icon-stop]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/stop.svg?ref_type=heads
[gl-icon-strikethrough]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/strikethrough.svg?ref_type=heads
[gl-icon-subgroup]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subgroup.svg?ref_type=heads
[gl-icon-subscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/subscript.svg?ref_type=heads
[gl-icon-substitute]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/substitute.svg?ref_type=heads
[gl-icon-superscript]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/superscript.svg?ref_type=heads
[gl-icon-symlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/symlink.svg?ref_type=heads
[gl-icon-table]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/table.svg?ref_type=heads
[gl-icon-tablet]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tablet.svg?ref_type=heads
[gl-icon-tachometer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tachometer.svg?ref_type=heads
[gl-icon-tag]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tag.svg?ref_type=heads
[gl-icon-tanuki-ai]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-ai.svg?ref_type=heads
[gl-icon-tanuki-verified]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki-verified.svg?ref_type=heads
[gl-icon-tanuki]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/tanuki.svg?ref_type=heads
[gl-icon-task-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/task-done.svg?ref_type=heads
[gl-icon-template]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/template.svg?ref_type=heads
[gl-icon-terminal]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terminal.svg?ref_type=heads
[gl-icon-terraform]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/terraform.svg?ref_type=heads
[gl-icon-text-description]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/text-description.svg?ref_type=heads
[gl-icon-thumb-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-down.svg?ref_type=heads
[gl-icon-thumb-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumb-up.svg?ref_type=heads
[gl-icon-thumbtack-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack-solid.svg?ref_type=heads
[gl-icon-thumbtack]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/thumbtack.svg?ref_type=heads
[gl-icon-time-out]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/time-out.svg?ref_type=heads
[gl-icon-timer]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/timer.svg?ref_type=heads
[gl-icon-title]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/title.svg?ref_type=heads
[gl-icon-todo-add]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-add.svg?ref_type=heads
[gl-icon-todo-done]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/todo-done.svg?ref_type=heads
[gl-icon-token]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/token.svg?ref_type=heads
[gl-icon-trend-down]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-down.svg?ref_type=heads
[gl-icon-trend-static]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-static.svg?ref_type=heads
[gl-icon-trend-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trend-up.svg?ref_type=heads
[gl-icon-trigger-source]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/trigger-source.svg?ref_type=heads
[gl-icon-unapproval]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unapproval.svg?ref_type=heads
[gl-icon-unassignee]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unassignee.svg?ref_type=heads
[gl-icon-underline]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/underline.svg?ref_type=heads
[gl-icon-unlink]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unlink.svg?ref_type=heads
[gl-icon-unstage-all]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/unstage-all.svg?ref_type=heads
[gl-icon-upgrade]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upgrade.svg?ref_type=heads
[gl-icon-upload]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/upload.svg?ref_type=heads
[gl-icon-user]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/user.svg?ref_type=heads
[gl-icon-users]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/users.svg?ref_type=heads
[gl-icon-volume-up]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/volume-up.svg?ref_type=heads
[gl-icon-warning-solid]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning-solid.svg?ref_type=heads
[gl-icon-warning]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/warning.svg?ref_type=heads
[gl-icon-weight]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/weight.svg?ref_type=heads
[gl-icon-work]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/work.svg?ref_type=heads
[gl-icon-x]: https://gitlab.com/zaira-ardor-llc/docs/-/raw/main/media/icons/gitlab/x.svg?ref_type=heads
